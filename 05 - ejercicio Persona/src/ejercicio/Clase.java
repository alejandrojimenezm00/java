package ejercicio;

import java.io.*;
import java.util.Scanner;


@SuppressWarnings("unused")
public class Clase {

	public static void main(String[] args) {

		@SuppressWarnings("resource")
		Scanner entrada = new Scanner(System.in);
		
		int opcion = 0;
		
		Persona[] tabla;
		tabla = new Persona[5];
		
		
		do 
		{

			int posicion = 0;
			int longCalle = 0;
			
			Persona alumnoCalle = null;
			String nombre, calle;
			int edad;
			
			
			System.out.println(	  "\t 1 -> Introducir un dato \n"
								+ "\t 2 -> Introducir toda la tabla \n"
								+ "\t 3 -> Imprimir tabla \n"
								+ "\t 4 -> Imprimir mayores de edad \n"
								+ "\t 5 -> Imprimir menores de edad \n"
								+ "\t 6 -> Imprimir alumno con la calle mas larga \n"
								+ "\t 7 -> salir");
			System.out.println("Opci�n: ");
			opcion = entrada.nextInt();
			entrada.nextLine();
			
			
			switch (opcion)
			{
			
				case 1:
				{
					if (posicion == 5)
					{
						System.out.println("La tabla esta completa, saliendo");
						break;
					}	
					
						System.out.println("Nombre: ");
						nombre = entrada.nextLine();
						System.out.println("Edad:");
						edad = entrada.nextInt();
						entrada.nextLine();
						System.out.println("Calle:");
						calle = entrada.nextLine();
						
						tabla[posicion] = new Persona(nombre, calle, edad);
						
						posicion++;
						break;
					}
				case 2:
				{
					for (int i = 5; posicion < i; posicion++)
					{
						System.out.println("Nombre: ");
						nombre = entrada.nextLine();
						System.out.println("Edad:");
						edad = entrada.nextInt();
						entrada.nextLine();
						System.out.println("Calle:");
						calle = entrada.nextLine();
						
						tabla[posicion] = new Persona(nombre, calle, edad);
						
					}
					break;

				}
				case 3:
				{
					for (int i = 0; i < posicion; i++)
						System.out.println(tabla[i]);
					break;
						
				}
				case 4:
				{
					for (int i = 0; i < posicion; i++)
						if (tabla[i].edad >= 18)
						{
							System.out.println("El alumno " + tabla[i].nombre + " es mayor de edad");
						}
					break;
				}
				case 5:
				{
					for (int i = 0; i < posicion; i++)
						if (tabla[i].edad < 18)
						{
							System.out.println("El alumno " + tabla[i].nombre + " es menor de edad");
						}
					break;
				}
				case 6:
				{
					for (int i = 0; i < posicion; i++)
						if (tabla[i].getCalle().length() > longCalle)
						{
							alumnoCalle = tabla[i];
						}
					System.out.println("El alumno que vive en la calle con mas caracter es: " + alumnoCalle.nombre);
					break;
				}
					
				}
			
			
			
		} while (opcion != 7 && opcion <= 7 && opcion > 0);
		
	}

}

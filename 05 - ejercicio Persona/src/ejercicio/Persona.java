package ejercicio;

public class Persona {

	String nombre;
	private String calle;
	int edad;
	
	public Persona(String n, String c, int e)
	{
		nombre = n;
		edad = e;
		calle = c;
	}
	
	
	
	public String getNombre() {
		return nombre;
	}



	public void setNombre(String nombre) {
		this.nombre = nombre;
	}



	public String getCalle() {
		return calle;
	}



	public void setCalle(String calle) {
		this.calle = calle;
	}



	public int getEdad() {
		return edad;
	}



	public void setEdad(int edad) {
		this.edad = edad;
	}



	public String escribir()
	{
		return "nombre: " + nombre + " | Edad: " +  edad + " | Calle: " + calle;
	}
	
}

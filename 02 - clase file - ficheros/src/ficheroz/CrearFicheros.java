package ficheroz;
import java.io.*;
import java.util.*;

public class CrearFicheros {

	public static void main(String[] args) {

		
		Scanner entrada = new Scanner(System.in);
		
		File fichero;
		String nombreFichero;
		
		
		System.out.println("Dime un nombre");
		nombreFichero = entrada.nextLine();
		
		
		fichero = new File(nombreFichero + ".txt");
		
		try 
		{
			fichero.createNewFile();
		} 
		catch (IOException e)
		{
			System.out.println("Error al crear el fichero");
		}
	}

}

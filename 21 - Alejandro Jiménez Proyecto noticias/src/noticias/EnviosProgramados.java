package noticias;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.time.LocalDate;
import java.util.Properties;
import java.util.TimerTask;

import javax.mail.Authenticator;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMessage.RecipientType;
import javax.swing.JOptionPane;

import com.sun.mail.util.SocketConnectException;

public class EnviosProgramados extends TimerTask{

	private String usuarioNombre;
	private String usuarioContrasena;
	private String usuarioMail;
	private LocalDate dia;
	
	public EnviosProgramados(String n, String c, String m, LocalDate d) 
	{
		usuarioNombre = n;
		usuarioContrasena = c;
		usuarioMail = m;
		dia = d;
		}

	public void run ()
	{	
		
		String nombreAdmin = "", contrasenaAdmin = "", mailAdmin = "", mailContrasena = "";
		
		LocalDate dia = LocalDate.now();
		
		
		try
		{
			BufferedReader br = new BufferedReader (new FileReader("usuarios.txt"));
			BufferedReader br2 = new BufferedReader (new FileReader("mailsconf.txt"));

			String linea = "", campo = "";
			int nCampo = 1;
			
			linea = br.readLine();
			String[] lineaConf = linea.split(";");
			
			nombreAdmin = lineaConf[2];
			contrasenaAdmin = lineaConf[3];
			
			br.close();
			
			br2.readLine();
			linea = br2.readLine();
			
			String[] mailConf = linea.split(";");
			mailAdmin = mailConf[0];
			mailContrasena = mailConf[1];

		}
		catch (IOException e)
		{
			System.out.println(e);
		}
		
		
	     String email = mailAdmin;
	     String contrasena = mailContrasena;
	  
	     Properties propiedadesConexion = new Properties();
	     
	     propiedadesConexion.put("mail.smtp.host", "smtp.gmail.com");
	     propiedadesConexion.put("mail.smtp.port", "587");
	     propiedadesConexion.put("mail.smtp.auth", "true");
	     propiedadesConexion.put("mail.smtp.starttls.enable", "true");
	     
	        Session sesion = Session.getInstance(propiedadesConexion,
	                new Authenticator() {
	                    protected PasswordAuthentication getPasswordAuthentication() {
	                        return new PasswordAuthentication(email, contrasena);
	                    }
	                });
	     
	     //compose the message  
	      try{  
	    	  String userConf = SacarNoticias.mostrarTitularesUsuario(usuarioNombre, usuarioContrasena);
	    	  String[] userConfig = userConf.split(";");
	    	  String titularesJuntitos = "";
	    	  String opcion = "";
	    	  
	    	  
	    	  for (int i = 1; i < userConfig.length; i++)
	    	  {
	    		opcion = userConfig[i];
	    		
	    		switch (opcion)
	    		{
	    		case "el Economista": 
	    							 titularesJuntitos = titularesJuntitos + SacarNoticias.mostrarTitularesEconomia1() + "\n"; 
	    							 break;
	    		case "Expansion": 
					 				 titularesJuntitos = titularesJuntitos + SacarNoticias.mostrarTitularesEconomia2() + "\n"; 
					 				 break;
	    		case "Bolsamania": 
	    						     titularesJuntitos = titularesJuntitos + SacarNoticias.mostrarTitularesEconomia3() + "\n"; 
	    						     break;
	    		case "Marca": 
	    							  titularesJuntitos = titularesJuntitos + SacarNoticias.mostrarTitularesDeportes1() + "\n"; 
	    							  break;
	    		case "as": 
	 				 				 titularesJuntitos = titularesJuntitos + SacarNoticias.mostrarTitularesDeportes2() + "\n"; 
	 				 				 break;
	    		case "Mundo Deportivo": 
	    						     titularesJuntitos = titularesJuntitos + SacarNoticias.mostrarTitularesDeportes3() + "\n"; 
	    						     break;
	    		case "Le Monde": 
	    							 titularesJuntitos = titularesJuntitos + SacarNoticias.mostrarTitularesInternacional1() + "\n"; 
	    							 break;
	    		case "The Times": 
	    						     titularesJuntitos = titularesJuntitos + SacarNoticias.mostrarTitularesInternacional2() + "\n"; 
	    						     break;
	    		case "BBC internacional": 
	    							 titularesJuntitos = titularesJuntitos + SacarNoticias.mostrarTitularesInternacional3() + "\n"; 
	    							 break;
					 
	    		}
	    	  }
	    	  
	    	  Message mail = new MimeMessage(sesion);
	    	  mail.setFrom(new InternetAddress(email));
	    	  mail.setRecipients(RecipientType.TO, InternetAddress.parse(usuarioMail));
	    	  
	    	  mail.setSubject("Titulares del d�a: " + dia);
	    	  mail.setText(titularesJuntitos);
	    	  
	    	  Transport.send(mail);
	    	  
	      } 
	      catch (AddressException e) 
	      {
				JOptionPane.showMessageDialog(null, "Se ha producido un error a la hora de realizar el envio al usuario "+ usuarioNombre + " Revise su informaci�n", "Error envio mail", 1);
	      }
	      catch (MessagingException mex) 
	      {
	    	 System.out.println(mex);
	      }


	}


	
}

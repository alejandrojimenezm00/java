package noticias;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

public class DatosEnvioMails {

	public static int calculoUsuarios ()
	{
		int numUsersTotal = 0;
		
		try {

			String linea;
			BufferedReader br = new BufferedReader(new FileReader("usuarios.txt"));
			
			while ((linea = br.readLine()) != null)
				numUsersTotal++;
			
			
		} catch (IOException e) {
			
			e.printStackTrace();
		}
		
		return numUsersTotal - 1;
	}
	
}

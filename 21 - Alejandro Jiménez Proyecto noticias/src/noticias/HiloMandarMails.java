package noticias;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

import javax.mail.internet.AddressException;

public class HiloMandarMails extends Thread {

	String horaEnvio;
	
	public HiloMandarMails (String n, String h)
	{
		super(n);
		horaEnvio = h;
	}
	
	public void run ()
	{
			MandarMailsAutomatico.MandarMails(horaEnvio);

	}
	
	public static String horaEnvio()
	{
		String hora = "";
		
		try {
			
			
			BufferedReader h = new BufferedReader(new FileReader("mailsconf.txt"));
			String fila = h.readLine();
			
			hora = fila;
			
			h.close();
			
		} catch (IOException e) 
		{
			e.printStackTrace();
		}
	
		return hora;
	}
}

package noticias;
import java.io.*;

import javax.swing.JTextField;


public class LoginFuncionalidad {

	
	public static int comprobarLogin(String nombre, String textFieldUser, String textFieldContra)
	{
		
		if (textFieldUser.equals("") || textFieldContra.equals(""))
			return 8;
		
		try
		{
			File f = new File(nombre);
			FileReader fr = new FileReader(f);
			BufferedReader br = new BufferedReader(fr);
			
			String linea;
			String campo = "", nombreL = "", contrasena = "", primerLogin = "", esAdmin = "";
			int nCampo = 1;
			
			linea = br.readLine();
			
			while (linea != null)
			{
				nCampo = 1;
				for (int i = 0; i < linea.length(); i++)
				{
					if (linea.charAt(i) != ';')
						campo += linea.charAt(i);
					else
					{
						switch (nCampo)
						{
						case 1: primerLogin = campo;
								break;
						case 2: esAdmin = campo;
								break;
						case 3: nombreL = campo;
								break;
						case 4: contrasena = campo;
								break;
						}
						nCampo++;
						campo = "";
					}
				}
				if (nombreL.equals(textFieldUser) && contrasena.equals(textFieldContra) && esAdmin.equals("1"))
				{
					if (nombreL.equals(textFieldUser) && contrasena.equals(textFieldContra))
						return 1;
				}
				if (nombreL.equals(textFieldUser) && contrasena.equals(textFieldContra) && primerLogin.equals("0"))
				{
					return 2;
				}
				if (nombreL.equals(textFieldUser) && contrasena.equals(textFieldContra) && primerLogin.equals("1"))
				{
					return 3;
				}
				linea = br.readLine();
			}
			br.close();
			
			
		}
		catch (IOException e)
		{
			System.out.println("Error al abrir el fichero");
		}
		return 4;
	}
	
}

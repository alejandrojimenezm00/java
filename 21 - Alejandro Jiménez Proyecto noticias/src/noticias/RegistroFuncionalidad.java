package noticias;
import java.io.*;

public class RegistroFuncionalidad {

	static public int registrarDatos (String nombre, String user, String contrasena, String mail) 
	{
		
		if (contrasena.contains(";") || user.contains(";") || mail.contains(";"))
			return 3;
		if (mail.contains("@") == false)
			return 4;
		
		try
		{
			
			int cont = 0;
			int nCampo;
			
			File f = new File(nombre);
			FileWriter fw = new FileWriter(f.getAbsoluteFile(), true);
			BufferedWriter bw = new BufferedWriter(fw);
			
			FileReader fr = new FileReader(f);
			BufferedReader br = new BufferedReader(fr);
			
			String linea = br.readLine();
			String campo = "", userName = "", userPass = "";
			
			
			bw.write("\n");
			while (linea != null)
			{
				nCampo = 1;
				for (int i = 0; i < linea.length(); i++)
				{
					if (linea.charAt(i) != ';')									// comprobar que el usuario no a�ade ; en el formulario
						campo += linea.charAt(i);								// si lo hace rompe el programa ergo suspendo
					else
					{
						switch (nCampo)
						{
						case 1: cont++;
								break;
						case 3: userName = campo;
								break;
						case 4: userPass = campo;
								break;
						}
						nCampo++;
						campo = "";
					}
					if (user.equals(userName) && contrasena.equals(userPass))
					{
						bw.close();
						return 5;
					}

				}				
				linea = br.readLine();

			}
			if (cont < 4)				
			{
				
				bw.write(String.valueOf(0));
				bw.write(";");
				bw.write(String.valueOf(cont+1));
				bw.write(";");
				bw.write(user);
				bw.write(";");
				bw.write(contrasena);
				bw.write(";");
				bw.write(mail);
				bw.write(";");

			}
			else
			{
				return 2;
			}
			bw.close();
		 
		}
		catch (IOException e)
		{
			System.out.println("error");
		}
		
		return 1;
		
	}
	
}

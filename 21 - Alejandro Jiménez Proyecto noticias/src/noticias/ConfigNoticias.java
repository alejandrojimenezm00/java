package noticias;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

public class ConfigNoticias {

	
	
	public static String cargaUsuarios ()
	{

		StringBuilder sb = new StringBuilder();
		int camposUser = 0;
		
		try
		{
					
			File f = new File("mailsconf.txt");
			FileReader fr = new FileReader(f);
			BufferedReader br = new BufferedReader(fr);
			
			String linea;
			String campo = "", numUser = "", mail = "";
			int nCampo = 1;
			
			
			linea = br.readLine();
			
			while (linea != null)
			{
				nCampo = 1;
				for (int i = 0; i < linea.length(); i++)
				{
					if (linea.charAt(i) != ';')
						campo += linea.charAt(i);
					else
					{
						switch (nCampo)
						{
						case 1: numUser = campo;
								sb.append(numUser + " ");
								camposUser++;
								break;
						case 3: mail = campo;
								sb.append(mail + "\n");
								camposUser++;
								break;
						}
						nCampo++;
						campo = "";
					}
				}
				linea = br.readLine();

			}
			br.close();

		} catch (IOException e)
		{
			System.out.println("error al cargar el fichero");
		}
				
		return sb.toString();
		
	}
	
	public static void configHoras (String[] h)
	{
		try
		{
			
			BufferedReader br = new BufferedReader(new FileReader("mailsconf.txt"));
			
			String linea = "", configUser = ""; 
			
			linea = br.readLine();
			configUser = br.readLine();
			
			br.close();
			
			BufferedWriter bw = new BufferedWriter(new FileWriter("mailsconf.txt"));
			
			bw.write(h[0]);
			bw.write(":");
			bw.write(h[1]);
			bw.write(":");
			bw.write(h[2]);
			bw.write(":\n");
			bw.write(configUser);

			bw.close();
			

			
		}
		catch (IOException e)
		{
			System.out.println(e);
		}
	}
	
	public static void cambiarNoticias(String[] n, String numUser)
	{
		
		try
		{
			
			BufferedReader br = new BufferedReader(new FileReader("usuariosconf.txt"));
			
			
			String nuevaLinea = numUser + ";" + "";
			String ficheroCambiar = "";
			String campoActual = "";
			String linea = "";
			String campo = "";
			String nUser = "";
			int nCampo = 0;
			
			for (int i = 0; i < n.length; i++)
			{
				nuevaLinea += n[i];
				nuevaLinea += ";";
			}
			
			while ((linea = br.readLine()) != null)
			{
				nCampo = 0;
				for (int i = 0; i < linea.length(); i++)
				{
					if (linea.charAt(i) != ';')									
						campo += linea.charAt(i);								
					else
					{
						switch (nCampo)
						{
						case 0: nUser = campo;
								break;
						}
						nCampo++;
						campo = "";
					}
					
				}
				
				if (nUser.equals(numUser))
					ficheroCambiar += nuevaLinea + "\n";
				else
					ficheroCambiar += linea + "\n";
				
			 }
			br.close();

			
			BufferedWriter bw = new BufferedWriter(new FileWriter("usuariosconf.txt"));
			bw.write(ficheroCambiar);
				
			bw.close();
			
			
		}
		catch (IOException e)
		{
			System.out.println(e);
		}
		
		
	}
	
	public static int numUsers ()
	{
		int numUsersTotal = 0;
		
		try
		{
			BufferedReader br = new BufferedReader(new FileReader("usuarios.txt"));
			String linea;
			
			while ((linea = br.readLine()) != null)
				numUsersTotal++;
			
		}
		catch (IOException e)
		{
			System.out.println(e);
		}
		
		return numUsersTotal-1;
	}
	
	public static String numUser(String userName, String userPass)
	{
		String numeroUsuarioBuscado = "";
		
		try
		{
			BufferedReader br = new BufferedReader(new FileReader("usuarios.txt"));
			
			String linea = "";
			String campo = "";
			String numFichero = "", userNameFichero = "", userPassFichero = "";
			int nCampo = 1;
			
			while (linea != null)
			{
				nCampo = 1;
				for (int i = 0; i < linea.length(); i++)
				{
					if (linea.charAt(i) != ';')
						campo += linea.charAt(i);
					else
					{
						switch (nCampo)
						{
						case 2: numFichero = campo;
								break;
						case 3: userNameFichero = campo;
								break;
						case 4: userPassFichero = campo;
								break;
						}
						nCampo++;
						campo = "";

					}
				}
				
				if (userNameFichero.equals(userName) && userPassFichero.equals(userPass))
				{
					numeroUsuarioBuscado = numFichero;
					return numFichero;
				}
				
				linea = br.readLine();
			}

		}
		catch (IOException e)
		{
			System.out.println(e);
		}
		
		return numeroUsuarioBuscado;
	}
	
}

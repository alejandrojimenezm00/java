package noticias;

import java.awt.BorderLayout;
import javax.swing.Timer;
import java.awt.EventQueue;
import java.awt.Image;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import java.awt.CardLayout;
import javax.swing.JProgressBar;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.net.URLConnection;
import java.awt.event.ActionEvent;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JLayeredPane;
import javax.mail.internet.AddressException;
import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JFormattedTextField;
import javax.swing.JPasswordField;
import javax.swing.JRadioButton;
import javax.swing.JTextArea;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JCheckBox;
import javax.swing.JMenuBar;

public class Noticias extends JFrame {

	private JPanel contentPane;
	static ImageIcon img = new ImageIcon("icono.jpg");
	
	
	

	Timer timer;
	int cargaValue = 0;
	private boolean buttonUser1 = false, buttonUser2 = false, buttonUser3 = false;
	private String[] titularesUser;
	private JTextField textFieldUsuarioLogin;
	private JPasswordField textFieldContrasenaLogin;
	private JTextField textFieldUsuarioRegis;
	private JPasswordField textFieldContrasenaRegis;
	private JTextField textFieldEmailRegis;
	private JTextField textFieldTitular1;
	private JTextField textFieldTitular2;
	private JTextField textFieldTitular3;
	private JTextField textFieldHoraEnvio;
	private JTextField textFieldMinutoEnvio;
	private JTextField textFieldSegundosEnvio;
	
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				
				if (!MandarMails.pruebaConexion())
					JOptionPane.showMessageDialog(null, "No dispones de conexi�n a internet \n revisa la conexi�n y vuelve a abrir la aplicacion", "carga", 1);
				else
				{
					MandarMailsAutomatico.creacionHilo();
				
					try {
						Noticias frame = new Noticias();
						frame.setVisible(true);
						frame.setIconImage(img.getImage());
						frame.setLocationRelativeTo(null);
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Noticias() {
		setTitle("El noticiero del Omnissiah");
		setResizable(false);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 723, 587);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(new CardLayout(0, 0));
		
		// Panel para la carga de la aplicacion
		
		JPanel panelCarga = new JPanel();
		contentPane.add(panelCarga, "name_354080407149600");
		panelCarga.setLayout(null);
		
		JLabel labelDibujito = new JLabel("");
		labelDibujito.setBounds(0, 0, 697, 538);
		panelCarga.add(labelDibujito);
		
		ImageIcon icon = new ImageIcon("221182160_622599512042214_3674847878831721369_n.jpg");
		Image image = icon.getImage();
		Image imagenEscalada = image.getScaledInstance(697, 538, java.awt.Image.SCALE_SMOOTH);
		icon = new ImageIcon (imagenEscalada);
		labelDibujito.setIcon(icon);
		
		// panel del login
		
		JPanel panelLogin = new JPanel();
		contentPane.add(panelLogin, "name_356707583588800");
		panelLogin.setLayout(null);
		
		// panel para ir a registrarse
		
		JPanel panelRegistro = new JPanel();
		contentPane.add(panelRegistro, "name_362457571814100");
		panelRegistro.setLayout(null);
		
		// panels configuracion admin
		
		JPanel panelGestionUsuarios = new JPanel();
		contentPane.add(panelGestionUsuarios, "name_76656615741800");
		panelGestionUsuarios.setLayout(null);
		
		JPanel panelGestionNoticias = new JPanel();
		contentPane.add(panelGestionNoticias, "name_76732008215800");
		panelGestionNoticias.setLayout(null);
		
		
		JPanel panelUserPrimerLogin = new JPanel();
		contentPane.add(panelUserPrimerLogin, "name_1568327018447600");
		panelUserPrimerLogin.setLayout(null);
		
		JPanel panelUserVerInfo = new JPanel();
		contentPane.add(panelUserVerInfo, "name_1571085946924600");
		panelUserVerInfo.setLayout(null);
		
		JLabel lblNewLabel_8 = new JLabel("Informaci\u00F3n sobre tu cuenta");
		lblNewLabel_8.setBounds(10, 11, 302, 47);
		panelUserVerInfo.add(lblNewLabel_8);
		
		JTextField textFIeldInfoUsuario = new JTextField();
		textFIeldInfoUsuario.setEnabled(false);
		textFIeldInfoUsuario.setEditable(false);
		textFIeldInfoUsuario.setBounds(10, 65, 677, 426);
		panelUserVerInfo.add(textFIeldInfoUsuario);
		
		JButton buttonSalir = new JButton("Salir");
		buttonSalir.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				textFieldUsuarioLogin.setText("");
				textFieldContrasenaLogin.setText("");
				panelLogin.setVisible(true);
				panelUserVerInfo.setVisible(false);
				
			}
		});
		buttonSalir.setBounds(10, 504, 89, 23);
		panelUserVerInfo.add(buttonSalir);
		
		
		textFieldMinutoEnvio = new JTextField();
		textFieldMinutoEnvio.setColumns(10);
		textFieldMinutoEnvio.setBounds(93, 58, 33, 20);
		panelGestionNoticias.add(textFieldMinutoEnvio);
		
		textFieldSegundosEnvio = new JTextField();
		textFieldSegundosEnvio.setColumns(10);
		textFieldSegundosEnvio.setBounds(93, 80, 33, 20);
		panelGestionNoticias.add(textFieldSegundosEnvio);
		
		
		textFieldHoraEnvio = new JTextField();
		textFieldHoraEnvio.setBounds(93, 33, 33, 20);
		panelGestionNoticias.add(textFieldHoraEnvio);
		textFieldHoraEnvio.setColumns(10);
		
		JLabel lblNewLabel_4 = new JLabel("Hora de envio");
		lblNewLabel_4.setBounds(10, 11, 116, 14);
		panelGestionNoticias.add(lblNewLabel_4);
		
		// boton configuracion de hora
		
		JButton buttonGuardConfigNoticias = new JButton("Guardar");
		buttonGuardConfigNoticias.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				String[] horaEnvio = {textFieldHoraEnvio.getText(), textFieldMinutoEnvio.getText(), textFieldSegundosEnvio.getText()};
				
				try 
				{
					int horaEnvioControl = Integer.parseInt(horaEnvio[0]);
					int minutoEnvioControl = Integer.parseInt(horaEnvio[1]);
					int segundoEnvioControl = Integer.parseInt(horaEnvio[2]);
				}
				catch (NumberFormatException e1)
				{
					JOptionPane.showMessageDialog(null, "No se permite la entrada de valores no numericos", "Error hora", 1);
				}
				
				if (horaEnvio[0].isEmpty() || horaEnvio[1].isEmpty() || horaEnvio[2].isEmpty())
					JOptionPane.showMessageDialog(null, "Rellena todos los campos", "Error hora", 1);
				else if (Integer.parseInt(textFieldHoraEnvio.getText()) > 24 || Integer.parseInt(textFieldHoraEnvio.getText()) < 0 || Integer.parseInt(textFieldMinutoEnvio.getText()) > 59 || Integer.parseInt(textFieldMinutoEnvio.getText()) < 0 || Integer.parseInt(textFieldSegundosEnvio.getText()) > 59 || Integer.parseInt(textFieldSegundosEnvio.getText()) < 0)
					JOptionPane.showMessageDialog(null, "Hora introducida incorrecta", "Error hora", 1);

				else
				{					
					ConfigNoticias.configHoras(horaEnvio);	
					MandarMailsAutomatico.creacionHilo();
					JOptionPane.showMessageDialog(null, "Hora cambiada", "cambio", 1);

				}
				
			}
		});
		buttonGuardConfigNoticias.setBounds(10, 111, 89, 23);
		panelGestionNoticias.add(buttonGuardConfigNoticias);

		
		JLabel lblNewLabel_5 = new JLabel("Hora");
		lblNewLabel_5.setBounds(10, 36, 73, 14);
		panelGestionNoticias.add(lblNewLabel_5);
		
		JLabel lblNewLabel_5_1 = new JLabel("Minuto");
		lblNewLabel_5_1.setBounds(10, 61, 73, 14);
		panelGestionNoticias.add(lblNewLabel_5_1);
		
		JLabel lblNewLabel_5_2 = new JLabel("Segudos");
		lblNewLabel_5_2.setBounds(10, 86, 73, 14);
		panelGestionNoticias.add(lblNewLabel_5_2);

		JPanel panelMenuAdmin = new JPanel();
		contentPane.add(panelMenuAdmin, "name_76396334768000");
		panelMenuAdmin.setLayout(null);
		
		// panels configuracion admin -> panel test
		
		JPanel panelTest = new JPanel();
		contentPane.add(panelTest, "name_81504263177500");
		panelTest.setLayout(null);
		
		// panels configuracion admin -> panel test -> partes para mostrar la informacion
		
		textFieldTitular1 = new JTextField();
		textFieldTitular1.setEditable(false);
		textFieldTitular1.setEnabled(false);
		textFieldTitular1.setBounds(10, 142, 677, 69);
		panelTest.add(textFieldTitular1);
		textFieldTitular1.setColumns(10);
		
		textFieldTitular2 = new JTextField();
		textFieldTitular2.setEditable(false);
		textFieldTitular2.setEnabled(false);
		textFieldTitular2.setColumns(10);
		textFieldTitular2.setBounds(10, 256, 677, 83);
		panelTest.add(textFieldTitular2);
		
		textFieldTitular3 = new JTextField();
		textFieldTitular3.setEditable(false);
		textFieldTitular3.setEnabled(false);
		textFieldTitular3.setColumns(10);
		textFieldTitular3.setBounds(10, 384, 677, 83);
		panelTest.add(textFieldTitular3);
		
		JLabel labelTitular1 = new JLabel("");
		labelTitular1.setBounds(10, 100, 677, 31);
		panelTest.add(labelTitular1);
		
		JLabel labelTitular2 = new JLabel("");
		labelTitular2.setBounds(10, 222, 677, 23);
		panelTest.add(labelTitular2);
		
		JLabel labelTitular3 = new JLabel("");
		labelTitular3.setBounds(10, 350, 677, 23);
		panelTest.add(labelTitular3);

		JLabel labelBienvenida = new JLabel("");
		labelBienvenida.setBounds(10, 11, 218, 14);
		panelUserPrimerLogin.add(labelBienvenida);
		
		// gestion usuario
		JLabel labelInfoUser = new JLabel("");
		labelInfoUser.setBounds(10, 70, 677, 14);
		panelGestionUsuarios.add(labelInfoUser);
		
		JCheckBox checkBoxElEconomista = new JCheckBox("El Economista");
		checkBoxElEconomista.setBounds(10, 150, 97, 23);
		panelGestionUsuarios.add(checkBoxElEconomista);
		
		JCheckBox checkBoxExpansion = new JCheckBox("Expansion");
		checkBoxExpansion.setBounds(10, 195, 97, 23);
		panelGestionUsuarios.add(checkBoxExpansion);
		
		JCheckBox checkBoxBolsamania = new JCheckBox("Bolsamania");
		checkBoxBolsamania.setBounds(10, 248, 97, 23);
		panelGestionUsuarios.add(checkBoxBolsamania);
		
		JCheckBox checkBoxMarca = new JCheckBox("Marca");
		checkBoxMarca.setBounds(158, 150, 97, 23);
		panelGestionUsuarios.add(checkBoxMarca);
		
		JCheckBox checkBoxAs = new JCheckBox("As");
		checkBoxAs.setBounds(158, 195, 97, 23);
		panelGestionUsuarios.add(checkBoxAs);
		
		JCheckBox checkBoxMundoDeportivo = new JCheckBox("Mundo Deportivo");
		checkBoxMundoDeportivo.setBounds(158, 248, 107, 23);
		panelGestionUsuarios.add(checkBoxMundoDeportivo);
		
		JCheckBox checkBoxBBC = new JCheckBox("BBC Internacional");
		checkBoxBBC.setBounds(288, 248, 126, 23);
		panelGestionUsuarios.add(checkBoxBBC);
		
		JCheckBox checkBoxTheTimes = new JCheckBox("The Times");
		checkBoxTheTimes.setBounds(288, 195, 97, 23);
		panelGestionUsuarios.add(checkBoxTheTimes);
		
		JCheckBox checkBoxLeMonde = new JCheckBox("Le Monde");
		checkBoxLeMonde.setBounds(288, 150, 97, 23);
		panelGestionUsuarios.add(checkBoxLeMonde);
		
		JLabel lblNewLabel_9 = new JLabel("Economia");
		lblNewLabel_9.setBounds(10, 129, 89, 14);
		panelGestionUsuarios.add(lblNewLabel_9);
		
		JLabel lblNewLabel_9_1 = new JLabel("Deportes");
		lblNewLabel_9_1.setBounds(158, 129, 89, 14);
		panelGestionUsuarios.add(lblNewLabel_9_1);
		
		JLabel lblNewLabel_9_2 = new JLabel("Internacional");
		lblNewLabel_9_2.setBounds(288, 129, 89, 14);
		panelGestionUsuarios.add(lblNewLabel_9_2);
		

		// funcionalidades de carga
		
		JProgressBar progressBarCarga = new JProgressBar(0, 100);
		progressBarCarga.setBounds(214, 372, 295, 23);
		progressBarCarga.setStringPainted(true);
		panelCarga.add(progressBarCarga);
		
		// boton para la barra de carga
		
		JButton buttonCarga = new JButton("Iniciar");
		buttonCarga.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				timer.start();
				
			}
		});
		
		timer = new Timer(150, new ActionListener() {			
			
			@Override
			public void actionPerformed(ActionEvent e) {
				progressBarCarga.setValue(cargaValue++);
				
				File f1 = new File("mailsconf.txt");
				File f2 = new File("usuarios.txt");
				File f3 = new File("usuariosconf.txt");
				
				
				if (cargaValue > 90)
				{
					if (f1.exists() || f2.exists() || f3.exists())
					{
						JOptionPane.showMessageDialog(null, "Ficheros cargados con exito", "carga", 1);
						panelLogin.setVisible(true);
						panelCarga.setVisible(false);
						timer.stop();
					}
					else
					{
						JOptionPane.showMessageDialog(null, "Ficheros no encontrados", "carga", 1);
						System.exit(0);
					}
				}
				

				
			}
		});
		buttonCarga.setBounds(321, 432, 89, 23);
		panelCarga.add(buttonCarga);
		

		
		
		// Funcionalidades Login
		

		textFieldUsuarioLogin = new JTextField();
		textFieldUsuarioLogin.setBounds(306, 184, 126, 20);
		panelLogin.add(textFieldUsuarioLogin);
		textFieldUsuarioLogin.setColumns(10);
	
		JLabel lblNewLabel = new JLabel("Usuario");
		lblNewLabel.setBounds(202, 187, 71, 14);
		panelLogin.add(lblNewLabel);
		
		JLabel lblNewLabel_1 = new JLabel("Contrase\u00F1a");
		lblNewLabel_1.setBounds(202, 218, 71, 14);
		panelLogin.add(lblNewLabel_1);
		
		textFieldContrasenaLogin = new JPasswordField();
		textFieldContrasenaLogin.setBounds(306, 215, 126, 20);
		panelLogin.add(textFieldContrasenaLogin);
		textFieldContrasenaLogin.setColumns(10);
		
		JLabel labelLoginControl = new JLabel("");
		labelLoginControl.setBounds(202, 317, 261, 14);
		panelLogin.add(labelLoginControl);
		
		// button Para el inicio de sesion
		
		JButton buttonIniciarSesion = new JButton("Iniciar Sesi\u00F3n");
		buttonIniciarSesion.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				
				String userConf;
				String titulares = "";
				int comprobarLogin = LoginFuncionalidad.comprobarLogin("usuarios.txt", textFieldUsuarioLogin.getText(), textFieldContrasenaLogin.getText());
				
				if (comprobarLogin == 1)
				{
					panelLogin.setVisible(false);
					panelMenuAdmin.setVisible(true);
				}
				else if (comprobarLogin == 2)
				{
					panelUserPrimerLogin.setVisible(true);
					panelLogin.setVisible(false);
				}
				else if (comprobarLogin == 3)
				{
					panelUserVerInfo.setVisible(true);
					userConf = SacarNoticias.mostrarTitularesUsuario(textFieldUsuarioLogin.getText(), textFieldContrasenaLogin.getText());
					String[] userConfig = userConf.split(";");
					
					textFIeldInfoUsuario.setEditable(true);
					for (int i = 1; i < userConfig.length; i++)
						titulares += userConfig[i] + "\n";
					textFIeldInfoUsuario.setText(titulares);
					textFIeldInfoUsuario.setEnabled(true);
					textFIeldInfoUsuario.setEditable(false);
					
 					panelLogin.setVisible(false);
				}
				else if(comprobarLogin == 8)
				{
					labelLoginControl.setText("No dejes los campos vacios");
				}
				else
					labelLoginControl.setText("Usuario no encontrado, registrate");
				
			}
		});
		buttonIniciarSesion.setBounds(306, 249, 126, 23);
		panelLogin.add(buttonIniciarSesion);
		
		// boton para cambiar al panel del registro
		
		JButton buttonIrRegistro = new JButton("Registro");
		buttonIrRegistro.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				textFieldUsuarioRegis.setText("");
				textFieldContrasenaRegis.setText("");
				panelLogin.setVisible(false);
				panelRegistro.setVisible(true);
				
			}
		});
		buttonIrRegistro.setBounds(306, 283, 126, 23);
		panelLogin.add(buttonIrRegistro);
		
		JButton buttonSalirAplicacion = new JButton("Salir");
		buttonSalirAplicacion.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				JOptionPane.showMessageDialog(null, "Adios, tenga un buen dia", "Salir", 1);

				System.exit(0);
			}
		});
		buttonSalirAplicacion.setBounds(10, 504, 89, 23);
		panelLogin.add(buttonSalirAplicacion);
		
		
		
		textFieldUsuarioRegis = new JTextField();
		textFieldUsuarioRegis.setColumns(10);
		textFieldUsuarioRegis.setBounds(288, 187, 126, 20);
		panelRegistro.add(textFieldUsuarioRegis);
		
		JLabel lblNewLabel_2 = new JLabel("Usuario");
		lblNewLabel_2.setBounds(207, 190, 71, 14);
		panelRegistro.add(lblNewLabel_2);
		
		JLabel lblNewLabel_1_1 = new JLabel("Contrase\u00F1a");
		lblNewLabel_1_1.setBounds(207, 215, 71, 14);
		panelRegistro.add(lblNewLabel_1_1);
		
		textFieldContrasenaRegis = new JPasswordField();
		textFieldContrasenaRegis.setColumns(10);
		textFieldContrasenaRegis.setBounds(288, 212, 126, 20);
		panelRegistro.add(textFieldContrasenaRegis);
		
		JLabel lblNewLabel_1_1_1 = new JLabel("Email");
		lblNewLabel_1_1_1.setBounds(207, 243, 71, 14);
		panelRegistro.add(lblNewLabel_1_1_1);
		
		textFieldEmailRegis = new JTextField();
		textFieldEmailRegis.setColumns(10);
		textFieldEmailRegis.setBounds(288, 240, 126, 20);
		panelRegistro.add(textFieldEmailRegis);

		JLabel labelRegistro = new JLabel("");
		labelRegistro.setBounds(173, 305, 326, 14);
		panelRegistro.add(labelRegistro);
		
		// boton para registrarse
		
		JButton buttonRegist = new JButton("Registro");
		buttonRegist.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				
				int controlRegistro = RegistroFuncionalidad.registrarDatos("usuarios.txt", textFieldUsuarioRegis.getText(), textFieldContrasenaRegis.getText(), textFieldEmailRegis.getText());
				
				if (controlRegistro == 1)
				{
					panelLogin.setVisible(true);
					panelRegistro.setVisible(false);
					labelLoginControl.setText("Usuario registrado");
				}
				else if (controlRegistro == 2)
				{
					panelLogin.setVisible(true);
					panelRegistro.setVisible(false);
					labelLoginControl.setText("Numero m�ximo de usuario registrados");
				}
				else if (controlRegistro == 3)
				{
					panelLogin.setVisible(true);
					panelRegistro.setVisible(false);
					labelLoginControl.setText("No se permite el uso de ; en el registro");
				}
				else if (controlRegistro == 4)
				{
					panelLogin.setVisible(true);
					panelRegistro.setVisible(false);
					labelLoginControl.setText("Por favor utilize un dominio de correo @gmail.com o hotmail.com");
				}
				else if (controlRegistro == 5)
				{
					panelLogin.setVisible(true);
					panelRegistro.setVisible(false);
					labelLoginControl.setText("El usuario que has ingresado ya se encuentra registrado");
				}
			}
		});
		buttonRegist.setBounds(288, 271, 126, 23);
		panelRegistro.add(buttonRegist);

		
		JLabel lblNewLabel_3 = new JLabel("Bienvenido Administador");
		lblNewLabel_3.setBounds(268, 269, 188, 36);
		panelMenuAdmin.add(lblNewLabel_3);
		
		JButton buttonIrConfigUsers = new JButton("Gestion Usuarios");
		buttonIrConfigUsers.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				panelGestionUsuarios.setVisible(true);
				panelMenuAdmin.setVisible(false);
			}
		});
		buttonIrConfigUsers.setBounds(268, 316, 144, 23);
		panelMenuAdmin.add(buttonIrConfigUsers);
		
		JButton buttonIrGestionNoticias = new JButton("Gestion Noticias");
		buttonIrGestionNoticias.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
								
				panelMenuAdmin.setVisible(false);				
				panelGestionNoticias.setVisible(true);
				
				
			}
		});
		buttonIrGestionNoticias.setBounds(268, 350, 144, 23);
		panelMenuAdmin.add(buttonIrGestionNoticias);
		
		JButton buttonIrTest = new JButton("Test");
		buttonIrTest.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				panelTest.setVisible(true);
				panelMenuAdmin.setVisible(false);
			}
		});
		buttonIrTest.setBounds(268, 384, 144, 23);
		panelMenuAdmin.add(buttonIrTest);
		
		JButton buttonGestionUsuariosRetroceder = new JButton("Volver");
		buttonGestionUsuariosRetroceder.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				panelMenuAdmin.setVisible(true);
				panelGestionUsuarios.setVisible(false);
				
			}
		});
		buttonGestionUsuariosRetroceder.setBounds(10, 504, 89, 23);
		panelGestionUsuarios.add(buttonGestionUsuariosRetroceder);
		
		JLabel lblNewLabel_6 = new JLabel("Configuraci\u00F3n de ususarios");
		lblNewLabel_6.setBounds(10, 11, 677, 14);
		panelGestionUsuarios.add(lblNewLabel_6);
		
		JButton buttonCargarUser1 = new JButton("Usuario1");
		buttonCargarUser1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				if (ConfigNoticias.numUsers() >= 1)
				{
					buttonUser1 = true;
					buttonUser2 = false;
					buttonUser3 = false;
	
					String[] user1 = SacarNoticias.sacarNumUser("2").split(";");
					titularesUser = SacarNoticias.mostrarTitularesUsuario(user1[2], user1[3]).split(";");
				

					for (int i = 1; i < titularesUser.length; i++)
					{
						switch (titularesUser[i])
						{
						case "el Economista": 
							checkBoxElEconomista.setSelected(true);
							break;
						case "Expansion":
							checkBoxExpansion.setSelected(true);
		    				break;
						case "Bolsamania":
							checkBoxBolsamania.setSelected(true);
							break;
						case "Marca": 
							checkBoxMarca.setSelected(true);
							break;
						case "as": 
							checkBoxAs.setSelected(true);
							break;
						case "Mundo Deportivo": 
							checkBoxMundoDeportivo.setSelected(true);
							break;
						case "Le Monde": 
							checkBoxLeMonde.setSelected(true);
							break;
						case "The Times": 
							checkBoxTheTimes.setSelected(true);
							break;
						case "BBC internacional": 
							checkBoxBBC.setSelected(true);
							break;
						}
					}
					
				}
				else
				{
					JOptionPane.showMessageDialog(null, "El usuario que has pulsado no existe", "carga", 1);
				}
			}
		});
		buttonCargarUser1.setBounds(10, 36, 89, 23);
		panelGestionUsuarios.add(buttonCargarUser1);
		
		JButton buttonCargarUser2 = new JButton("Usuario 2");
		buttonCargarUser2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				if (ConfigNoticias.numUsers() >= 2)
				{
					buttonUser1 = false;
					buttonUser2 = true;
					buttonUser3 = false;
		
					String[] user1 = SacarNoticias.sacarNumUser("3").split(";");
					titularesUser = SacarNoticias.mostrarTitularesUsuario(user1[2], user1[3]).split(";");
					
					for (int i = 1; i < titularesUser.length; i++)
					{
						switch (titularesUser[i])
						{
			    		case "el Economista": 
			    			checkBoxElEconomista.setSelected(true);
							 break;
			    		case "Expansion":
			    			checkBoxExpansion.setSelected(true);
			 				 break;
			    		case "Bolsamania":
			    			checkBoxBolsamania.setSelected(true);
						     break;
			    		case "Marca": 
			    			checkBoxMarca.setSelected(true);
							  break;
			    		case "as": 
			 				 break;
			    		case "Mundo Deportivo": 
			    			checkBoxMundoDeportivo.setSelected(true);
						     break;
			    		case "Le Monde": 
			    			checkBoxLeMonde.setSelected(true);
							 break;
			    		case "The Times": 
			    			checkBoxTheTimes.setSelected(true);
						     break;
			    		case "BBC internacional": 
			    			checkBoxBBC.setSelected(true);
							 break;
						}
					}
				}
				else
				{
					JOptionPane.showMessageDialog(null, "El usuario que has pulsado no existe", "carga", 1);
				}
				
			}
		});
		buttonCargarUser2.setBounds(158, 36, 89, 23);
		panelGestionUsuarios.add(buttonCargarUser2);
		
		JButton buttonCargarUser3 = new JButton("Usuario 3");
		buttonCargarUser3.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				if (ConfigNoticias.numUsers() >= 3)
				{
					buttonUser1 = false;
					buttonUser2 = false;
					buttonUser3 = true;
		
					String[] user1 = SacarNoticias.sacarNumUser("4").split(";");
					titularesUser = SacarNoticias.mostrarTitularesUsuario(user1[2], user1[3]).split(";");
					
					for (int i = 1; i < titularesUser.length; i++)
					{
						switch (titularesUser[i])
						{
			    		case "el Economista": 
			    			checkBoxElEconomista.setSelected(true);
							 break;
			    		case "Expansion":
			    			checkBoxExpansion.setSelected(true);
			 				 break;
			    		case "Bolsamania":
			    			checkBoxBolsamania.setSelected(true);
						     break;
			    		case "Marca": 
			    			checkBoxMarca.setSelected(true);
							  break;
			    		case "as": 
			 				 break;
			    		case "Mundo Deportivo": 
			    			checkBoxMundoDeportivo.setSelected(true);
						     break;
			    		case "Le Monde": 
			    			checkBoxLeMonde.setSelected(true);
							 break;
			    		case "The Times": 
			    			checkBoxTheTimes.setSelected(true);
						     break;
			    		case "BBC internacional": 
			    			checkBoxBBC.setSelected(true);
							 break;
						}
					}
				}
				else
				{
					JOptionPane.showMessageDialog(null, "El usuario que has pulsado no existe", "carga", 1);
				}
				
			}
		});
		buttonCargarUser3.setBounds(288, 36, 89, 23);
		panelGestionUsuarios.add(buttonCargarUser3);

		
		JButton buttonGuardarNuevosTitulares = new JButton("Guardar");
		buttonGuardarNuevosTitulares.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				if (buttonUser1 == true || buttonUser2 == true || buttonUser3 == true)
				{
					String[] titularesSeleccionados;
					String totalTitulares = "";
					
					if (checkBoxElEconomista.isSelected())
					{
						totalTitulares += "El Economista";
						totalTitulares += ";";
					}
					if (checkBoxExpansion.isSelected())
					{
						totalTitulares += "Expansion";
						totalTitulares += ";";
					}
					if (checkBoxBolsamania.isSelected())
					{
						totalTitulares += "Bolsamania";
						totalTitulares += ";";
					}
					if (checkBoxMarca.isSelected())
					{
						totalTitulares += "Marca";
						totalTitulares += ";";
					}
					if (checkBoxAs.isSelected())
					{
						totalTitulares += "as";
						totalTitulares += ";";
					}
					if (checkBoxMundoDeportivo.isSelected())
					{
						totalTitulares += "Mundo Deportivo";
						totalTitulares += ";";
					}
					if (checkBoxLeMonde.isSelected())
					{
						totalTitulares += "Le Monde";
						totalTitulares += ";";
					}
					if (checkBoxTheTimes.isSelected())
					{
						totalTitulares += "The Times";
						totalTitulares += ";";
					}
					if (checkBoxBBC.isSelected())
					{
						totalTitulares += "BBC internacional";
						totalTitulares += ";";
					}
					
					titularesSeleccionados = totalTitulares.split(";");
					
					if (buttonUser1 == true)
					{ 
						ConfigNoticias.cambiarNoticias(titularesSeleccionados, "2");
					}
					if (buttonUser2 == true)
					{
						ConfigNoticias.cambiarNoticias(titularesSeleccionados, "3");
					}
					if (buttonUser3 == true)
					{
						ConfigNoticias.cambiarNoticias(titularesSeleccionados, "4");
					}
				}
				else
				{
					JOptionPane.showMessageDialog(null, "Selecciona al menos un usuario", "carga", 1);
				}
				
			}
		});
		buttonGuardarNuevosTitulares.setBounds(158, 309, 89, 23);
		panelGestionUsuarios.add(buttonGuardarNuevosTitulares);
		
		
		JButton buttonCerrarSesion = new JButton("Salir");
		buttonCerrarSesion.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				panelMenuAdmin.setVisible(false);
				textFieldContrasenaLogin.setText("");
				textFieldUsuarioLogin.setText("");
				panelLogin.setVisible(true);
				
			}
		});
		buttonCerrarSesion.setBounds(10, 504, 89, 23);
		panelMenuAdmin.add(buttonCerrarSesion);
		

		// Funcionalidades test
		
		JButton buttonSalirTest = new JButton("Salir");
		buttonSalirTest.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				panelTest.setVisible(false);
				panelMenuAdmin.setVisible(true);
				
			}
		});
		buttonSalirTest.setBounds(10, 504, 89, 23);
		panelTest.add(buttonSalirTest);
		
		
		// boton para los titulares economicos
		
		JButton buttonMostrarTitularesEconomia = new JButton("Economia");
		buttonMostrarTitularesEconomia.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				textFieldTitular1.setEditable(true);
				textFieldTitular2.setEditable(true);
				textFieldTitular3.setEditable(true);
				
				labelTitular1.setText("El economista");
				textFieldTitular1.setText(SacarNoticias.mostrarTitularesEconomia1());
				textFieldTitular1.setEnabled(true);
				labelTitular2.setText("Expansion");
				textFieldTitular2.setText(SacarNoticias.mostrarTitularesEconomia2());
				textFieldTitular2.setEnabled(true);
				labelTitular3.setText("Bolsamania");
				textFieldTitular3.setText(SacarNoticias.mostrarTitularesEconomia3());
				textFieldTitular2.setEnabled(true);
				textFieldTitular1.setEditable(false);
				textFieldTitular2.setEditable(false);
				textFieldTitular3.setEditable(false);
				
			}
		});
		
		buttonMostrarTitularesEconomia.setBounds(10, 8, 182, 47);
		panelTest.add(buttonMostrarTitularesEconomia);
		
		
		
		// Boton titulares deportes
		
		JButton buttonMostrarTitularesDeportes = new JButton("Deportes");
		buttonMostrarTitularesDeportes.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				textFieldTitular1.setEditable(true);
				textFieldTitular2.setEditable(true);
				textFieldTitular3.setEditable(true);
				labelTitular1.setText("Marca");
				textFieldTitular1.setText(SacarNoticias.mostrarTitularesDeportes1());
				textFieldTitular1.setEnabled(true);
				labelTitular2.setText("as");
				textFieldTitular2.setText(SacarNoticias.mostrarTitularesDeportes2());
				textFieldTitular2.setEnabled(true);
				labelTitular3.setText("Mundo deportivo");
				textFieldTitular3.setText(SacarNoticias.mostrarTitularesDeportes3());
				textFieldTitular3.setEnabled(true);
				textFieldTitular1.setEditable(false);
				textFieldTitular2.setEditable(false);
				textFieldTitular3.setEditable(false);
				
				
			}
		});
		buttonMostrarTitularesDeportes.setBounds(263, 8, 192, 47);
		panelTest.add(buttonMostrarTitularesDeportes);
		
		
		
		// boton para los titulares internacionales
		
		JButton buttonMostrarTitularesInternacional = new JButton("Internacional");
		buttonMostrarTitularesInternacional.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				textFieldTitular1.setEditable(true);
				textFieldTitular2.setEditable(true);
				textFieldTitular3.setEditable(true);
				labelTitular1.setText("Le Monde");
				textFieldTitular1.setText(SacarNoticias.mostrarTitularesInternacional1());
				textFieldTitular1.setEnabled(true);
				labelTitular2.setText("The Times");
				textFieldTitular2.setText(SacarNoticias.mostrarTitularesInternacional2());
				textFieldTitular2.setEnabled(true);
				labelTitular3.setText("BBC internacional");
				textFieldTitular3.setText(SacarNoticias.mostrarTitularesInternacional3());
				textFieldTitular3.setEnabled(true);
				textFieldTitular1.setEditable(false);
				textFieldTitular2.setEditable(false);
				textFieldTitular3.setEditable(false);
				
			}
		});
		buttonMostrarTitularesInternacional.setBounds(515, 8, 172, 47);
		panelTest.add(buttonMostrarTitularesInternacional);
		
		JButton buttonMandarMails = new JButton("Probar envio");
		buttonMandarMails.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			
				if (MandarMails.pruebaConexion())
					try
					{
						MandarMails.pruebaMandadoMails();

					}
					catch (AddressException e1)
					{
						System.out.println("mail no encontrado");
					}
				else
					JOptionPane.showMessageDialog(null, "No dispones de conexi�n a internet \n revisa la conexi�n y vuelve a abrir la aplicacion", "carga", 1);
				
				
			}
		});
		buttonMandarMails.setBounds(554, 504, 133, 23);
		panelTest.add(buttonMandarMails);

		JLabel lblNewLabel_7 = new JLabel("Selecciona los periodicos que quieres consultar");
		lblNewLabel_7.setBounds(10, 40, 279, 40);
		panelUserPrimerLogin.add(lblNewLabel_7);
		
		JLabel lblNewLabel_7_1 = new JLabel("Economia");
		lblNewLabel_7_1.setBounds(10, 73, 279, 40);
		panelUserPrimerLogin.add(lblNewLabel_7_1);
		
		JRadioButton rdbtnEconomista = new JRadioButton("El economista");
		rdbtnEconomista.setEnabled(false);
		rdbtnEconomista.setBounds(10, 120, 109, 23);
		panelUserPrimerLogin.add(rdbtnEconomista);
		
		JRadioButton rdbtnExpansion = new JRadioButton("Expansion");
		rdbtnExpansion.setEnabled(false);
		rdbtnExpansion.setBounds(10, 146, 109, 23);
		panelUserPrimerLogin.add(rdbtnExpansion);
		
		JRadioButton rdbtnBolsamania = new JRadioButton("Bolsamania");
		rdbtnBolsamania.setEnabled(false);
		rdbtnBolsamania.setBounds(10, 172, 109, 23);
		panelUserPrimerLogin.add(rdbtnBolsamania);
		
		JLabel lblNewLabel_7_1_1 = new JLabel("Deportes");
		lblNewLabel_7_1_1.setBounds(255, 73, 279, 40);
		panelUserPrimerLogin.add(lblNewLabel_7_1_1);
		
		JRadioButton rdbtnMarca = new JRadioButton("Marca");
		rdbtnMarca.setEnabled(false);
		rdbtnMarca.setBounds(255, 120, 109, 23);
		panelUserPrimerLogin.add(rdbtnMarca);
		
		JRadioButton rdbtnAs = new JRadioButton("as");
		rdbtnAs.setEnabled(false);
		rdbtnAs.setBounds(255, 146, 109, 23);
		panelUserPrimerLogin.add(rdbtnAs);
		
		JRadioButton rdbtnMundoDeportivo = new JRadioButton("Mundo Deportivo");
		rdbtnMundoDeportivo.setEnabled(false);
		rdbtnMundoDeportivo.setBounds(255, 172, 109, 23);
		panelUserPrimerLogin.add(rdbtnMundoDeportivo);
		
		JLabel lblNewLabel_7_1_1_1 = new JLabel("Intenacional");
		lblNewLabel_7_1_1_1.setBounds(543, 73, 279, 40);
		panelUserPrimerLogin.add(lblNewLabel_7_1_1_1);
		
		JRadioButton rdbtnLeMonde = new JRadioButton("Le Monde");
		rdbtnLeMonde.setEnabled(false);
		rdbtnLeMonde.setBounds(543, 120, 109, 23);
		panelUserPrimerLogin.add(rdbtnLeMonde);
		
		JRadioButton rdbtnTheTimes = new JRadioButton("The Times");
		rdbtnTheTimes.setEnabled(false);
		rdbtnTheTimes.setBounds(543, 146, 109, 23);
		panelUserPrimerLogin.add(rdbtnTheTimes);
		
		JRadioButton rdbtnTheStar = new JRadioButton("BBC internacional");
		rdbtnTheStar.setEnabled(false);
		rdbtnTheStar.setBounds(543, 172, 109, 23);
		panelUserPrimerLogin.add(rdbtnTheStar);
		
		JButton buttonGuardarPrimerLogin = new JButton("Guardar configuracion");
		buttonGuardarPrimerLogin.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				int contarPosicion = 0;
				boolean seleccion = false;
				String[] botonesSeleccionados = new String[9];
				
				// Comprobacion si los botones se han pulsado
				
				
				// Periodicos de economia
				if (rdbtnEconomista.isSelected())
				{
					botonesSeleccionados[contarPosicion] = "el Economista";
					contarPosicion++;
					seleccion = true;
				}
				if (rdbtnExpansion.isSelected())
				{
					botonesSeleccionados[contarPosicion] = "Expansion";
					contarPosicion++;
					seleccion = true;
				}
				if (rdbtnBolsamania.isSelected())
				{
					botonesSeleccionados[contarPosicion] = "Bolsamania";
					contarPosicion++;
					seleccion = true;
				}
				
				// Periodicos de deportes
				if (rdbtnMarca.isSelected())
				{
					botonesSeleccionados[contarPosicion] = "Marca";
					contarPosicion++;
					seleccion = true;
				}
				if (rdbtnAs.isSelected())
				{
					botonesSeleccionados[contarPosicion] = "as";
					contarPosicion++;
					seleccion = true;
				}
				if (rdbtnMundoDeportivo.isSelected())
				{
					botonesSeleccionados[contarPosicion] = "Mundo Deportivo";
					contarPosicion++;
					seleccion = true;
				}
				
				// Periodicos internacionales
				if (rdbtnLeMonde.isSelected())
				{
					botonesSeleccionados[contarPosicion] = "Le Monde";
					contarPosicion++;
					seleccion = true;
				}
				if (rdbtnTheTimes.isSelected())
				{
					botonesSeleccionados[contarPosicion] = "The Times";
					contarPosicion++;
					seleccion = true;
				}
				if (rdbtnTheStar.isSelected())
				{
					botonesSeleccionados[contarPosicion] = "BBC internacional";
					contarPosicion++;
					seleccion = true;
				}
				
				
				if (primerLoginConfig.guardarPrimerLogin(botonesSeleccionados, contarPosicion) == 0 && seleccion == true)
				{
					primerLoginConfig.guardarAjustes(textFieldUsuarioLogin.getText(), textFieldContrasenaLogin.getText());
					JOptionPane.showMessageDialog(null, "Configuraci�n guardada, vuelva al login para iniciar sesi�n", "Exito", 1);
					rdbtnEconomista.setEnabled(false);
					rdbtnExpansion.setEnabled(false);
					rdbtnBolsamania.setEnabled(false);
					rdbtnMarca.setEnabled(false);
					rdbtnAs.setEnabled(false);
					rdbtnMundoDeportivo.setEnabled(false);
					rdbtnLeMonde.setEnabled(false);
					rdbtnTheTimes.setEnabled(false);
					rdbtnTheStar.setEnabled(false);

					
				}
					
				
			}
		});
		buttonGuardarPrimerLogin.setBounds(241, 262, 151, 23);
		panelUserPrimerLogin.add(buttonGuardarPrimerLogin);
		
		JButton buttonSalirPrimerLogin = new JButton("Salir");
		buttonSalirPrimerLogin.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				panelLogin.setVisible(true);
				panelUserPrimerLogin.setVisible(false);
				
			}
		});
		buttonSalirPrimerLogin.setBounds(10, 504, 89, 23);
		panelUserPrimerLogin.add(buttonSalirPrimerLogin);

		JCheckBox checkBoxMostrarEconomia = new JCheckBox("");
		checkBoxMostrarEconomia.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				if (checkBoxMostrarEconomia.isSelected())
				{
					rdbtnEconomista.setEnabled(true);
					rdbtnExpansion.setEnabled(true);
					rdbtnBolsamania.setEnabled(true);
				}
				if (!checkBoxMostrarEconomia.isSelected())
				{
					rdbtnEconomista.setEnabled(false);
					rdbtnExpansion.setEnabled(false);
					rdbtnBolsamania.setEnabled(false);
				}
				
			}
		});
		checkBoxMostrarEconomia.setBounds(62, 82, 97, 23);
		panelUserPrimerLogin.add(checkBoxMostrarEconomia);

		JCheckBox checkBoxMostrarDeportes = new JCheckBox("");
		checkBoxMostrarDeportes.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				if (checkBoxMostrarDeportes.isSelected())
				{
					rdbtnMarca.setEnabled(true);
					rdbtnAs.setEnabled(true);
					rdbtnMundoDeportivo.setEnabled(true);
				}
				if (!checkBoxMostrarDeportes.isSelected())
				{
					rdbtnMarca.setEnabled(false);
					rdbtnAs.setEnabled(false);
					rdbtnMundoDeportivo.setEnabled(false);
				}
				
			}
		});
		checkBoxMostrarDeportes.setBounds(306, 82, 97, 23);
		panelUserPrimerLogin.add(checkBoxMostrarDeportes);
		
		
		JCheckBox checkBoxMostrarInternacional = new JCheckBox("");
		checkBoxMostrarInternacional.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				if (checkBoxMostrarInternacional.isSelected())
				{
					rdbtnLeMonde.setEnabled(true);
					rdbtnTheTimes.setEnabled(true);
					rdbtnTheStar.setEnabled(true);
				}
				if (!checkBoxMostrarInternacional.isSelected())
				{
					rdbtnLeMonde.setEnabled(false);
					rdbtnTheTimes.setEnabled(false);
					rdbtnTheStar.setEnabled(false);
				}
				
			}
		});
		checkBoxMostrarInternacional.setBounds(610, 82, 97, 23);
		panelUserPrimerLogin.add(checkBoxMostrarInternacional);

		
		JButton buttonGestionNoticiasRetroceder = new JButton("Volver");
		buttonGestionNoticiasRetroceder.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				panelGestionNoticias.setVisible(false);
				panelMenuAdmin.setVisible(true);
				
			}
		});
		buttonGestionNoticiasRetroceder.setBounds(10, 504, 89, 23);
		panelGestionNoticias.add(buttonGestionNoticiasRetroceder);
		
		
		JButton buttonVolverLogin = new JButton("Volver");
		buttonVolverLogin.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				panelLogin.setVisible(true);
				textFieldUsuarioRegis.setText("");
				textFieldContrasenaRegis.setText("");
				textFieldEmailRegis.setText("");
				panelRegistro.setVisible(false);
			}
		});
		buttonVolverLogin.setBounds(10, 504, 89, 23);
		panelRegistro.add(buttonVolverLogin);
	}
}

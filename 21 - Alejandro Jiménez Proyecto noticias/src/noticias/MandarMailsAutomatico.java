package noticias;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.sql.*;
import java.text.*;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.*;
import java.util.Date;

import javax.mail.internet.AddressException;
import javax.management.timer.Timer.*;


public class MandarMailsAutomatico {

	public static void creacionHilo()
	{
		String hora = HiloMandarMails.horaEnvio();
		HiloMandarMails hilo = new HiloMandarMails("hilo", hora);
		hilo.start();
	}
	
	public static void MandarMails(String h)
	{

		EnviosProgramados[] envios = new EnviosProgramados[3];
		LocalDate dia = LocalDate.now();

		
		DateFormat formatoHora = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		
		try {
			
			Date fechaEnvio = formatoHora.parse(dia + " " + h);
			Timer envio = new Timer();
			
			
			BufferedReader user = new BufferedReader(new FileReader("usuarios.txt"));
			
			String campo = "", linea;
			String [][] users = new String[4][3];
			
			int nCampo = 1, campoUser = 0, campoPass = 0, numUser = 0;
			
			// comprobar los usuarios existentes en el fichero
		
			while ((linea = user.readLine()) != null)
			{
				nCampo = 1;
				
				for (int i = 0; i < linea.length(); i++)
				{
					if (linea.charAt(i) != ';')									
						campo += linea.charAt(i);
					else
					{
						switch (nCampo)
						{
						case 3:
								users[numUser][0] = campo;
								break;
						case 4:
								users[numUser][1] = campo;
								break;
						case 5:
								users[numUser][2] = campo;
								break;
						}
						nCampo++;
						campo = "";
					}
					
				}
				
				numUser++;
				campoUser = 0;
			
				
			}
						
			user.close();
			
			
			for (int i = 1; i < numUser; i++)
			{
				envio.schedule(new EnviosProgramados(users[i][0], users[i][1], users[i][2], dia), fechaEnvio);
			}
			
			
		}
		catch (ParseException e) 
		{
			e.printStackTrace();
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
	}
	
}

package noticias;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

public class SacarNoticias {
	
	static String mostrarTitularesDeportes1()
	{
		String cadena = null;
		Document document = null;            
		String webPage = "https://www.marca.com/"; 
			try {
				document = Jsoup.connect(webPage).get();
			} catch (IOException e) {
				e.printStackTrace();
			}	
	        Element palabra2 = document.getElementsByClass("mod-title").select("a").first();
	        cadena=palabra2.html().toUpperCase();
		return cadena;
	}
	
	static String mostrarTitularesDeportes2()
	{
		String cadena = null;
		Document document = null;            
		String webPage = "https://as.com/"; 
			try {
				document = Jsoup.connect(webPage).get();
			} catch (IOException e) {
				e.printStackTrace();
			}	
	        Element palabra2 = document.getElementsByClass("title").select("a").first();
	        cadena=palabra2.html().toUpperCase();
		return cadena;
	}
	
	static String mostrarTitularesDeportes3()
	{
		String cadena = null;
		Document document = null;            
		String webPage = "https://www.mundodeportivo.com/"; 
			try {
				document = Jsoup.connect(webPage).get();
			} catch (IOException e) {
				e.printStackTrace();
			}	
	        Element palabra2 = document.getElementsByClass("title").select("a").first();
	        cadena=palabra2.html().toUpperCase();
		return cadena;
	}
	
	
	
	
	
	
	
	
	// Titulares Economia
	
	
	static String mostrarTitularesEconomia1()
	{
		String cadena = null;
		Document document = null;            
		String webPage = "https://www.eleconomista.es/"; 
			try {
				document = Jsoup.connect(webPage).get();
			} catch (IOException e) {
				e.printStackTrace();
			}	
	        Element palabra2 = document.getElementsByClass("h1").select("center").first();
	        cadena=palabra2.html().toUpperCase();
		return cadena;
	}
	
	static String mostrarTitularesEconomia2()
	{
		String cadena = null;
		Document document = null;            
		String webPage = "https://www.expansion.com/"; 
			try {
				document = Jsoup.connect(webPage).get();
			} catch (IOException e) {
				e.printStackTrace();
			}	
	        Element palabra2 = document.getElementsByClass("ue-c-cover-content__headline").select("a").first();
	        cadena=palabra2.html().toUpperCase();
		return cadena;
	}
	
	static String mostrarTitularesEconomia3()
	{
		String cadena = null;
		Document document = null;            
		String webPage = "https://www.bolsamania.com/"; 
			try {
				document = Jsoup.connect(webPage).get();
			} catch (IOException e) {
				e.printStackTrace();
			}	
	        Element palabra2 = document.getElementsByClass("bloque1 ").select("a").first();
	        cadena=palabra2.html().toUpperCase();
		return cadena;
	}

	
	// Titulares internacionales
	
	static String mostrarTitularesInternacional1()
	{
		String cadena = null;
		Document document = null;            
		String webPage = "https://www.lemonde.fr/"; 													// periodico de Francia
			try {
				document = Jsoup.connect(webPage).get();
			} catch (IOException e) {
				e.printStackTrace();
			}	
	        Element palabra2 = document.getElementsByClass("article__title-label").select("p").first();
	        cadena=palabra2.html().toUpperCase();
		return cadena;
	}
	
	static String mostrarTitularesInternacional2()
	{
		String cadena = null;
		Document document = null;            
		String webPage = "https://www.thetimes.co.uk/"; 													// periodico de UK
			try {
				document = Jsoup.connect(webPage).get();
			} catch (IOException e) {
				e.printStackTrace();
			}	
	        Element palabra2 = document.getElementsByClass("Item-headline Headline--xl").select("a").first();
	        cadena=palabra2.html().toUpperCase();
		return cadena;
	}
	
	static String mostrarTitularesInternacional3()
	{
		String cadena = null;
		Document document = null;            
		String webPage = "https://www.bbc.com/mundo/topics/c2lej05epw5t"; 										
			try {
				document = Jsoup.connect(webPage).get();
			} catch (IOException e) {
				e.printStackTrace();
			}	
	        Element palabra2 = document.getElementsByClass("qa-heading-link lx-stream-post__header-link").select("span").first();
	        cadena=palabra2.html().toUpperCase();
		return cadena;
	}

	
	static String sacarNumUser(String num)
	{
		String numUser = "";
		
		try
		{
			String linea = "";
			String campo = "";
			String numBuscado = "";
			int nCampo = 1;
			
			BufferedReader br = new BufferedReader(new FileReader("usuarios.txt"));

			while ((linea =  br.readLine()) != null)
			{
				nCampo = 0;
				for (int i = 0; i < linea.length(); i++)
				{
					if (linea.charAt(i) != ';')									// comprobar que el usuario no a�ade ; en el formulario
						campo += linea.charAt(i);								// si lo hace rompe el programa ergo suspendo
					else
					{
						switch (nCampo)
						{
						case 1:
							numUser = campo;
							break;

						}
						nCampo++;
						campo = "";
					}
					
					if (numUser.equals(num))
						return linea;
						
				}
		
			}
			br.close();
		}
		catch (IOException e)
		{
			System.out.println(e);
		}
		
		return "";
	}
	
	
	static String mostrarTitularesUsuario(String n, String c)
	{
		String cadena = "";
		String posicion = "";
		String nombre = "", contrasena = "";
		
		try {
		
			BufferedReader br = new BufferedReader(new FileReader("usuarios.txt"));
			BufferedReader br2 = new BufferedReader(new FileReader("usuariosconf.txt"));
			
			String linea = "";
			String campoBuscado = "";
			
			int nCampo = 0;
			
			while ((linea =  br.readLine()) != null)
			{
				nCampo = 0;
				for (int i = 0; i < linea.length(); i++)
				{
					if (linea.charAt(i) != ';')									// comprobar que el usuario no a�ade ; en el formulario
						campoBuscado += linea.charAt(i);								// si lo hace rompe el programa ergo suspendo
					else
					{
						switch (nCampo)
						{
						case 1:
							posicion = campoBuscado;
							break;
						case 2:
							nombre = campoBuscado;
							break;
						case 3:
							contrasena = campoBuscado;
							break;
						}
						nCampo++;
						campoBuscado = "";
					}
					
					if (nombre.equals(n) && contrasena.equals(c))
					{
						cadena = linea;
						break;
					}
				}
		
			}
			
			while ((linea = br2.readLine()) != null)
			{
				nCampo = 0;
				for (int i = 0; i < linea.length(); i++)
				{
					if (linea.charAt(i) != ';')									// comprobar que el usuario no a�ade ; en el formulario
						campoBuscado += linea.charAt(i);								// si lo hace rompe el programa ergo suspendo
					else
					{
						switch (nCampo)
						{
						case 0:
							if (posicion.equals(campoBuscado))
								return cadena = linea;
							break;
						}
						
						nCampo++;
						campoBuscado = "";
						
					}
				}

			}
			br.close();
			br2.close();
		
		
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		
		return "";
	}


}

package noticias;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.RandomAccessFile;

public class primerLoginConfig {

	public static int guardarPrimerLogin(String[] botonesSeleccionados, int posicionesARecorrer)
	{
		try
		{
			
			int cont = 0;
			int nCampo;
			
			File f = new File("usuariosconf.txt");
			FileWriter fw = new FileWriter(f.getAbsoluteFile(), true);
			BufferedWriter bw = new BufferedWriter(fw);
			
			FileReader fr = new FileReader(f);
			BufferedReader br = new BufferedReader(fr);
			
			
			String linea = br.readLine();
			String campo = "";
			while (linea != null)
			{
				nCampo = 1;
				for (int i = 0; i < linea.length(); i++)
				{
					if (linea.charAt(i) != ';')									// comprobar que el usuario no a�ade ; en el formulario
						campo += linea.charAt(i);								// si lo hace rompe el programa ergo suspendo
					else
					{
						switch (nCampo)
						{
						case 1: cont++;
								break;
						}
						nCampo++;
					}
				}
				linea = br.readLine();
			}
			if (cont < 4)				
			{
				bw.write("\n");
				bw.write(String.valueOf(cont+1)+";");
				for (int i = 0; i < posicionesARecorrer; i++)
				{
					bw.write(botonesSeleccionados[i]);
					bw.write(";");
				}
			}

			bw.close();
		 
		}
		catch (IOException e)
		{
			System.out.println("error");
		}
		
		return 0;
	}
	
	
	public static void guardarAjustes(String usuario, String contrasena) 
	{
	      
		      
			File f = new File("usuarios.txt");
		    FileWriter fr = null;
			String linea, ficheroACambiar = "";

		    String ficheroNuevo = "", lineaCambiada = "";
			try {
			
		    	BufferedReader br = new BufferedReader(new FileReader(f));
		    	
		    	while ((linea = br.readLine()) != null)
		    	{
		    		String[] lineaCambiar = linea.split(";");
		    		
		    		if (lineaCambiar[1].equals(ConfigNoticias.numUser(usuario, contrasena)))
		    		{
		    			lineaCambiar[0] = "1";
		    			for (int i = 0; i < lineaCambiar.length; i++)
		    				lineaCambiada += lineaCambiar[i] + ";";
		    			ficheroNuevo += lineaCambiada;
		    		}
		    		else
		    		{
		    			ficheroNuevo += linea;
		    		}
			    		
		    		//ficheroACambiar = ficheroACambiar + linea + "\n";
		    		ficheroNuevo += "\n";
		    	}
		    	
		    	br.close();

		    	ficheroNuevo = ficheroNuevo.substring(0, ficheroNuevo.length() - 1);
		    	
		    	fr = new FileWriter(f);
		    	fr.write(ficheroNuevo);
		    	fr.close();
		    	

				
			} 
		    catch (IOException e) 
		    {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
	       
	       
	}

	
	
	
}

package ventana;
import javax.swing.*;
import java.awt.*;


public class Gui01 extends JFrame{

	
	
	public Gui01()
	{

		setLayout(new BorderLayout(5, 10));
		add (new JButton ("1"), BorderLayout.EAST);
		add (new JButton ("2"), BorderLayout.SOUTH);
		add (new JButton ("3"), BorderLayout.WEST);
		add (new JButton ("4"), BorderLayout.NORTH);
		add (new JButton ("5"), BorderLayout.CENTER);
		
		setSize(200, 200);
		
		setVisible(true);
		setDefaultCloseOperation(EXIT_ON_CLOSE);

	}
	
	public static void main(String[] args) {
		Gui01 aplication = new Gui01();
		
	}

}

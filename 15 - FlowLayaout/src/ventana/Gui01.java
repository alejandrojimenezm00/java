package ventana;
import javax.swing.*;
import java.awt.*;


public class Gui01 extends JFrame{

	
	
	public Gui01()
	{

		setLayout (new FlowLayout(FlowLayout.LEFT, 10, 20));
		for (int i = 1; i <= 10; i++)
		{
			add(new JButton("Componente " + i));
		}
		
		setSize(200, 200);
		setVisible(true);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
	}
	
	public static void main(String[] args) {
		Gui01 aplication = new Gui01();
		
	}

}

package buffer;

import java.io.*;

public class Buffer {

	
	public static void EscribirDatos(String nombre, String[] tabla)
	{
		try
		{
			// Primero creamos el BufferedWriter
			File              f = new File(nombre);
			FileWriter       fw = new FileWriter(f);
			BufferedWriter 	 bw = new BufferedWriter(fw);
			
			// Escribir los datos
			
			for (int i = 0; i < tabla.length; i++)
			{
				bw.write(tabla[i]);
				bw.newLine();
			}
			
			// Cerrar el fichero
			
			bw.close();
			
		}
		catch (IOException e)
		{
			System.out.println("Error al escribir" + e.getMessage());
		}
		catch (NullPointerException e)
		{
			System.out.println("Error al crear el fichero" + e.getMessage());
		}
	}
	
	
	public static void LeerDatos(String nombre, String[] tabla)
	{
		try
		{
			// Primero creamos el BufferedWriter
			File              f = new File(nombre);
			FileReader       fr = new FileReader(f);
			BufferedReader 	 br = new BufferedReader(fr);
			String linea;
			int posicion = 0;
			
			// Lectura de datos
			linea = br.readLine();
			while (linea != null)
			{
				tabla[posicion] = linea;
				posicion++;
				linea = br.readLine();

			}
			
			// Cerrar el fichero
			
			br.close();
			
		}
		catch (IOException e)
		{
			System.out.println("Error al leer" + e.getMessage());
		}
		catch (NullPointerException e)
		{
			System.out.println("Error al leer el fichero" + e.getMessage());
		}
	}
	
	public static void EscribirTabla(String[] tabla)
	{
		for (int i = 0; i < 3; i++)
			System.out.println(tabla[i]);
	}
	
	public static void main(String[] args)
	{

		String[] misNombres = {"Pepe", "Ana", "Luis"};
		String[] otraTabla = new String[3];
		
		EscribirDatos("Personas.txt", misNombres);
		LeerDatos("Personas.txt", otraTabla);
		EscribirTabla(otraTabla);
		

		
	}

}

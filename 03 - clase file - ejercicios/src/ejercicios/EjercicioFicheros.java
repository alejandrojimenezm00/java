package ejercicios;
import java.io.*;
import java.util.*;

public class EjercicioFicheros {

	public static void main(String[] args) {

		Scanner entrada = new Scanner(System.in);
		
		File carpeta;
		String nombreFichero, ruta;
		int repeticiones;
		
		System.out.println("Ahora dime la ruta");
		ruta = entrada.nextLine();
		
		System.out.println("Dime un nombre para guardar datos");
		nombreFichero = entrada.nextLine();
		
		System.out.println("Ahora dime cuantos ficheros quieres");
		repeticiones = entrada.nextInt();
		
		carpeta = new File(ruta);
		carpeta.mkdir();
	
		
		try
		{
			File fichero;
			for (int i = 0; i <= repeticiones; i++)
			{
				fichero = new File (ruta + "/" + nombreFichero + i + ".txt");
				fichero.createNewFile();
			}
		}
		catch (IOException e)
		{
			System.out.println("Error al crear el fichero");
		}
		
	
		entrada.close();
		
	}

}

package areas;

import java.util.Scanner;

public class calculo {

	public static void main(String[] args) {

		@SuppressWarnings("resource")
		Scanner entrada = new Scanner(System.in);
		int opcion = 0;
		
		String color;
		String tamano;
		double radio, lado, dMayor, dMenor, bMayor, bMenor, altura, base;
		
		do {
			
			System.out.println("1 -> Circulo\n"
					          +"2 -> Cuadrado\n"
					          +"3 -> Triangulo\n"
					          +"4 -> Rombo\n"
					          +"5 -> Trapecio\n"
					          +"6 -> Rectangulo\n"
					          +"7 -> Salir\n"
					          +"Opcion:");
			opcion = entrada.nextInt();
			entrada.nextLine();
			
			switch (opcion)
			{
			case 1:
			{
				System.out.println("Color:");
				color = entrada.nextLine();
				System.out.println("Tama�o: ");
				tamano = entrada.nextLine();
				System.out.println("Radio: ");
				radio = entrada.nextDouble();
				entrada.nextLine();
				
				System.out.println(Circulo.calcularArea(color, tamano, radio));
				break;
			}
			case 2:
			{
				System.out.println("Color:");
				color = entrada.nextLine();
				System.out.println("Tama�o: ");
				tamano = entrada.nextLine();
				System.out.println("Lado: ");
				lado = entrada.nextDouble();
				entrada.nextLine();
				
				System.out.println(Cuadrado.calcularArea(color, tamano, lado));
				break;
			}
			case 3:
			{
				System.out.println("Color:");
				color = entrada.nextLine();
				System.out.println("Tama�o: ");
				tamano = entrada.nextLine();
				System.out.println("Base: ");
				base = entrada.nextDouble();
				entrada.nextLine();
				System.out.println("Altura: ");
				altura = entrada.nextDouble();
				entrada.nextLine();
				
				System.out.println(Triangulo.calcularArea(color, tamano, base, altura));
				break;
			}
			case 4:
			{
				System.out.println("Color:");
				color = entrada.nextLine();
				System.out.println("Tama�o: ");
				tamano = entrada.nextLine();
				System.out.println("Diagonal Mayor: ");
				dMayor = entrada.nextDouble();
				entrada.nextLine();
				System.out.println("Diagonal Menor: ");
				dMenor = entrada.nextDouble();
				entrada.nextLine();
				
				System.out.println(Rombo.calcularArea(color, tamano, dMayor, dMenor));
				break;
			}
			case 5:
			{
				System.out.println("Color:");
				color = entrada.nextLine();
				System.out.println("Tama�o: ");
				tamano = entrada.nextLine();
				System.out.println("Base Mayor: ");
				bMayor = entrada.nextDouble();
				entrada.nextLine();
				System.out.println("Base Menor: ");
				bMenor = entrada.nextDouble();
				entrada.nextLine();
				System.out.println("Altura: ");
				altura = entrada.nextDouble();
				entrada.nextLine();
				
				System.out.println(Trapecio.calcularArea(color, tamano, bMayor, bMenor, altura));
				break;
			}
			case 6:
			{
				System.out.println("Color:");
				color = entrada.nextLine();
				System.out.println("Tama�o: ");
				tamano = entrada.nextLine();
				System.out.println("Base: ");
				base = entrada.nextDouble();
				entrada.nextLine();
				System.out.println("Altura: ");
				altura = entrada.nextDouble();
				entrada.nextLine();
				
				System.out.println(Rectangulo.calcularArea(color, tamano, base, altura));
				break;
			}
			}
			
		} while (opcion != 7 && opcion <= 6 && opcion > 0);
		
	}

}

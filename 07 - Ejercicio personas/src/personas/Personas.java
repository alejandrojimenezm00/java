package personas;

public class Personas {

	String nombre;
	String apellido;
	String direccion;
	int edad;
	
	// constructores
	
	public Personas (String n, String a, String d, int e) // constructor para todos los datos
	{
		n = nombre;
		a = apellido;
		d = direccion;
		e = edad;
	}
	
	public Personas (String d) // constructor para la direccion
	{
		d = direccion;
		nombre = "";
		apellido = "";
		edad = 0;
	}
	
	public Personas () // constructor sin parametros
	{
		nombre = "";
		apellido = "";
		edad = 0;
		direccion = "";
	}
	
	
	// demas metodos
	

	public String obtenerNombre ()
	{
		return nombre + " " + apellido;
	}
	
	public int obtenerEdad ()
	{
		return edad;
	}
	
	public  String obtenerDireccion ()
	{
		return direccion;
	}
	
	
	public boolean mayorEdad ()
	{
		if (edad >= 18)
			return true;
		else
			return false;
		
		// return (edad >= 18); manera mas rapida de devovler funciones booleanas
	}
	
	public String escribirNombre ()
	{
		return "El alumno se llama " + nombre + " sus apellidos son " + apellido + " vive en " + direccion + " y tiene " + edad + " a�os";
	}
	
	public String escribirApellidos ()
	{
		return "El alumno se apellida " + apellido + "su nombre es: " + nombre +" vive en " + direccion + " y tiene " + edad + " a�os";
	}
	
	public String escribirFichero ()
	{
		return nombre + ";" + apellido + ";" + direccion + ";" + edad + ";";
	}

	
	
	
	
}

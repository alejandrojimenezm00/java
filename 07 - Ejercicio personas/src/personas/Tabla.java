package personas;

import java.util.Scanner;
import java.io.*;

public class Tabla {

	@SuppressWarnings("unused")
	public static void main(String[] args) {
	
		Scanner entrada = new Scanner(System.in);
		
		
		Personas[] tabla;
		tabla = new Personas[10];
		Personas[] indices;
		indices = new Personas[10];
		
		int opcion = 0;
		int posicion = 0;																		// la variable posicion la utilizo como herramienta de control. Con ella
																								// compruebo en todo momento en que posicion de la tabla se encuentra el 
																								// programa
		
		
		do 
		{
			System.out.println("1  -> crear la tabla\n"
					         + "2  -> Leer datos\n"
					         + "3  -> Escribir datos de la tabla\n"
					         + "4  -> Mayor de edad\n"
					         + "5  -> Mayor direccion\n"
					         + "6  -> Eliminar posicion\n"
					         + "7  -> Eliminar posiciones\n"
					         + "8  -> Insertar posicion\n"
					         + "9  -> Insertar posiciones\n"
					         + "10 -> Crear tabla de indice de nombres\n"
					         + "11 -> Escribir tabla de inidice de nombres\n"
					         + "12 -> Crear fichero\n"
					         + "13 -> Leer fichero\n"
					         + "14 -> Salir\n"
					         + "Opci�n:");
			
			opcion = entrada.nextInt();
			entrada.nextLine();
		
		
		
		switch (opcion)
		{
		case 1:
		{
			tabla = new Personas[10];
			for (int i = 0; i < 10; i++)
				tabla[i] = new Personas();
			break;
		}
		case 2:
		{	
			if (posicion > 10)
			{
				System.out.println("la tabla esta llena");
				break;
			}
			

			System.out.println("Nombre: ");
			tabla[posicion].nombre = entrada.nextLine();
			System.out.println("Apellidos: ");
			tabla[posicion].apellido = entrada.nextLine();
			System.out.println("Edad: ");
			tabla[posicion].edad = entrada.nextInt();
			entrada.nextLine();
			System.out.println("Direcci�n: ");
			tabla[posicion].direccion = entrada.nextLine();
			
			posicion++;
			break;
		}
		case 3:
		{
			
			for (int i = 0; i < posicion; i++)
				if (tabla[i] != null)
					System.out.println(i+1 + " " + tabla[i].escribirNombre());
			
			break;
		}
		case 4:
		{
			int maEdad = 0;
			int peEdad = 0;
			
			for (int i = 0; i < posicion; i++)
				if (tabla[i].edad > maEdad)
				{
					peEdad = i;
					maEdad = tabla[i].edad;
				}
					
			System.out.println("Estos son los datos de la persona mas mayor de la tabla");
			System.out.println(tabla[peEdad].escribirNombre());
			break;
		}
		case 5:
		{
			int maDireccion = 0;
			int peDireccion = 0;
			
			for (int i = 0; i < posicion; i++)
				if (tabla[i].direccion.length() > maDireccion)
				{
					peDireccion = i;
					maDireccion = tabla[i].direccion.length();
				}
			System.out.println("Estos son los datos de la persona con la direccion mas larga");
			System.out.println(tabla[peDireccion].escribirNombre());
			break;
		}
		case 6:
		{
			
			if (posicion == 0)
			{		
				System.out.println("No hay datos que eliminar");
				break;
			}
			
			int eleccion;
			int eleccion2;
			System.out.println("Dime una celda que quieras eliminar");
			eleccion = entrada.nextInt();
			entrada.nextLine();
			eleccion2 = eleccion;
			eleccion--; 												// Especifico un decremento por si el usuario no cuenta desde el 0
																		// De esta forma se accede a la columna anterior a la que realmente
																		// se accederia con lo que marque el usuario
			
			for (int i = 0; i < posicion; i++)
				tabla[eleccion + i] = tabla[eleccion2++];
			
			posicion--;
		
			break;
		}
		case 7:
		{
			int posicion1, posicion2;
			int aux;
			
			System.out.println("Dima la posicion inicial:");
			posicion1 = entrada.nextInt();
			entrada.nextLine();
			System.out.println("Dima la posicion Final:");
			posicion2 = entrada.nextInt();
			entrada.nextLine();
			
			
			posicion1--;
			posicion2--;
			aux = posicion2+1;
			
			for (int i = posicion1; i <= posicion2; i++)
			{
				tabla[i] = tabla[aux++];								// He planteado la soluci�n a este ejercicio, no como una eliminacion como tal de los datos
				posicion--;												// sino mas como una suplantaci�n. Me explico, en vez de eliminar los datos, piso los que existan
			}															// con los siguientes al rango del usuario, restando a su vez posiciones recorridas en la tabla.
																		// con esto he conseguido no tener que dejar ningun campo a null y que a la hora de a�adir datos 
			break;														// se suplanten los de las posiciones existentes. Existe redundancia de datos que no soy capaz 
		}																// de solventar, sin embargo, esta ser�a trasparente para el usuario y no la notaria.
		case 8:
		{
			int posicionDada;
			
			System.out.println("Dime que posicion quieres a�adir");		// En este caso entiendo que un usuario que quiera a�adir una columna en medio de la tabla
			posicionDada = entrada.nextInt();							// es porque quiere a�adir datos en la misma, por ello, aumento la variable posicion, desplazo
			entrada.nextLine();											// todas las celdas hacia la derecha y en la localizaci�n donde el usuario quiere a�adir datos
																		// Creo una persona vacia que el propio usuario rellenar�.
			posicionDada--;
			
			
			
			for (int i = posicion; i > posicionDada; i--)
			{
				posicion++;
				tabla[i] = tabla[i-1];
			}
				
			tabla[posicionDada] = new Personas();
			
			
			System.out.println("Nombre: ");
			tabla[posicionDada].nombre = entrada.nextLine();
			System.out.println("Apellidos: ");
			tabla[posicionDada].apellido = entrada.nextLine();
			System.out.println("Edad: ");
			tabla[posicionDada].edad = entrada.nextInt();
			entrada.nextLine();
			System.out.println("Direcci�n: ");
			tabla[posicionDada].direccion = entrada.nextLine();
			

			break;
		}
		case 9:
		{
			int posicion1, posicion2;
			int diff, aux;
			
			System.out.println("Dima la posicion inicial:");
			posicion1 = entrada.nextInt();
			entrada.nextLine();
			System.out.println("Dima la posicion Final:");
			posicion2 = entrada.nextInt();
			entrada.nextLine();
			
			posicion1--;
			posicion2--;														// En esta parte, primero desplazo la variable posicion para usarla como referencia
																				// Despues pongo en las posiciones creadas las personas de las posiciones i - diff,
			diff = posicion2 - posicion1;										// de esta forma me aseguro el desplazamiento
			aux = posicion1 + 1;												// una vez hecho eso realizo los mismos pasos que el caso 8, creo Personas vacias
																				// y le pido al usuario que las rellene.
			for (int i = 0; i < diff; i++)
				posicion++;
			
			for (int i = posicion2 + 1; i < posicion; i++)
				tabla[i] = tabla[i-diff];
			
			for (int i = posicion1 + 1; i <= posicion2; i++)
			{
				tabla[i] = new Personas();
				
				System.out.println("Nombre: ");
				tabla[i].nombre = entrada.nextLine();
				System.out.println("Apellidos: ");
				tabla[i].apellido = entrada.nextLine();
				System.out.println("Edad: ");
				tabla[i].edad = entrada.nextInt();
				entrada.nextLine();
				System.out.println("Direcci�n: ");
				tabla[i].direccion = entrada.nextLine();
			
			}

						
			break;	
			
		}
		case 10:
		{
			indices = new Personas[posicion];
			
			for (int i = 0; i < posicion; i++)
				indices[i] = new Personas(null);
			
			for (int i = 0; i < posicion; i++)
				indices[i].nombre = tabla[i].nombre;
			
			break;
		}
		case 11:
		{
			for (int i = 0; i < posicion; i++)
				System.out.println(indices[i].nombre);
			
			break;
		}
		
		case 12:
		{
			try {
			
			File f = new File("personas.txt");
			FileWriter fw = new FileWriter(f);
			
			
			for (int i = 0; i < posicion; i++)
			{
				fw.write(tabla[i].escribirFichero());
			
			fw.write('\r');  
			fw.write('\n');  
			}
			
			fw.close();
		
			}
			catch (IOException e)
			{
				System.out.println("Error al crear el fichero" + e);
			}
			
		}
		
		case 13:
		{
			try
			{
			File f = new File("personas.txt");
			FileReader fr = new FileReader(f);
			int caracter;
			String valor = "";
			// char[] letra = new char[20];
			
			int contador = 0;
			
			// 2. Lectura l�nea a l�nea de cada nombre
			caracter = fr.read();
			
			while (caracter != -1)
			{
				if (caracter == '\n')
				{
					tabla[contador] = new Personas(valor);						
					contador++;
					posicion++;
					valor = "";										
				}
				
				if (caracter != '\n')
				{
					valor += (char) caracter;						
					
				}
				
				caracter = fr.read();								
			}
			
			fr.close();
			}
			catch (IOException e)
			{
				System.out.println("Error al escribir " + e);
			}
		}
		
		}
		
		} while (opcion != 14 && opcion <= 14 && opcion > 0);
		
	}

}

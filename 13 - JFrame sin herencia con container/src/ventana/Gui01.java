package ventana;
import javax.swing.*;
import java.awt.*;


public class Gui01{

	private Container panel;
	private JButton miboton;
	
	
	public Gui01()
	{
		JFrame frame = new JFrame ("Ejemplo1");

		panel = frame.getContentPane();
		
		miboton = new JButton("Aceptar");
		panel.add(miboton);
		
		frame.pack();
		frame.setVisible(true);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

	}
	
	public static void main(String[] args) {
		Gui01 aplication = new Gui01();
		
	}

}

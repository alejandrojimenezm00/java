package repasoJoseLuis;

public class UsaPersona {

	public static void main(String[] args) 
	{
		// Declaracion de las variables tipo persona
		Persona persona1, persona2;
		char letra;
		
		
		// Inicializacion de los objetos
		persona2 = new Persona();
		persona1 = new Persona("Antonio", 23);
		
		// llamada al metodo DNI de Persona
		letra = Persona.letraDNI(123);

		
		
		System.out.println(persona1.escribir() + letra);
		System.out.println(persona2.escribir());
		persona1.setEdad(28);
		System.out.println(persona1.escribir());
		
	}

}

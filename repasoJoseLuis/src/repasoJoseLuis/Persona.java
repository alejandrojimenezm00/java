package repasoJoseLuis;

public class Persona 
{

	// Atributos de la clase
	
	private String nombre;
	private int    edad;
	
	public static char letraDNI (int dni)
	{
		if (dni < 10)
			return 'R';
		else
			return 'G';
	}
	
	// Metodos de la clase
	
	public Persona (String n, int e)
	{
		nombre = n;
		edad = e;
	}
	
	// Constructor sin parametros para campos por defecto
	public Persona () 
	{
		nombre = "Sin nombre";
		edad = -1;
	}
	
	// Setter de la edad
	
	public void setEdad (int e)
	{
		edad = e;
	}
	
	// Setter del nombre
	
	public void serNombre (String n)
	{
		nombre = n;
	}
	
	// Metodo para devolver una cadena
	
	public String escribir()
	{
		return "Nombre " + nombre + " Edad: " + edad;
	}

}

package tienda;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;


public class Main {

	
	public static int numProductos ()
	{
		
		int numProductos = 0;
		
		try
		{
			
			BufferedReader br = new BufferedReader(new FileReader("productos.txt"));
			
			String linea = "";
			
			while ((linea = br.readLine()) != null)
				numProductos++;
			
		}
		catch (IOException e)
		{
			System.out.println(e);
		}
		
		return numProductos;
		
	}
	public static int numCompras()
	{
		
		int numProductos = 0;
		
		try
		{
			
			BufferedReader br = new BufferedReader(new FileReader("compras.txt"));
			
			String linea = "";
			
			while ((linea = br.readLine()) != null)
				numProductos++;
			
		}
		catch (IOException e)
		{
			System.out.println(e);
		}
		
		return numProductos;
		
	}
	
	
	public static void main(String[] args) {
		
		
		
		Producto[] tablaProductos = new Producto[numProductos()];
		Compras[] tablaCompras = new Compras[numCompras()];
		
		try
		{
			BufferedReader br = new BufferedReader(new FileReader("productos.txt"));
			
			int contador = 0;
			String precio = "", codProducto = "";
			String linea = "", campo = "";
			int nCampo = 0;
			
			while ((linea = br.readLine()) != null)
			{
				
				nCampo = 0;
				
				for (int i = 0; i < linea.length(); i++)
				{
					
					if (linea.charAt(i) != ';')
					{
						campo += linea.charAt(i);
					}
					else
					{
						
						switch (nCampo)
						{
						case 0: codProducto = campo;
								break;
						case 1: precio = campo;
								break;
						}
						
						nCampo++;
						campo = "";
						
					}
					
					
				}
				
				tablaProductos[contador] = new Producto(Integer.parseInt(codProducto), Integer.parseInt(precio));
				contador++;
				
				
			}
			
			
		}
		catch (IOException e)
		{
			System.out.println(e);
		}
		
		
		try
		{
			BufferedReader br = new BufferedReader(new FileReader("compras.txt"));
			int contador = 0;
			String idCompra = "", cantidad = "";
			String linea = "", campo = "";
			int nCampo = 0;
			
			while ((linea = br.readLine()) != null)
			{
				
				nCampo = 0;
				
				for (int i = 0; i < linea.length(); i++)
				{
					
					if (linea.charAt(i) != ';')
					{
						campo += linea.charAt(i);
					}
					else
					{
						
						switch (nCampo)
						{
						case 0: idCompra = campo;
								break;
						case 2: cantidad = campo;
								break;
						}
						
						nCampo++;
						campo = "";
						
					}
					
					
				}
				tablaCompras[contador] = new Compras(Integer.parseInt(idCompra), tablaProductos[contador], Integer.parseInt(cantidad));
				contador++;
				
				
			}
			
			
		}
		catch (IOException e)
		{
			System.out.println(e);
		}
		
		
		for (int i = 0; i < tablaCompras.length; i++)
			System.out.println(tablaCompras[i].devolverCompra());
		
		
	}

}

package tienda;

public class Compras {

	private int codigoCompra;
	private Producto producto;
	private int cantidad;
	
	public Compras (int cC, Producto cP, int c)
	{
		codigoCompra = cC;
		producto = cP;
		cantidad = c;
	}

	public int devolverPrecioCompra ()
	{
		int totalDinero = 0;
		
		totalDinero = producto.devolverPrecio() * cantidad;
		
		return totalDinero;
	}
	
	public int cantidadMenosVendida ()
	{
		return cantidad;
	}
	
	
	public String devolverCompra()
	{
		return codigoCompra + ";" + producto.devovlerCodigo() + ";" + cantidad;
	}
}

package tienda;

public class Producto {

	private int codigoProducto;
	private int precio;
	
	public Producto (int cP, int p)
	{
		codigoProducto = cP;
		precio = p;
	}
	
	
	public int devolverPrecio ()
	{
		return precio;
	}
	
	public int devovlerCodigo()
	{
		return codigoProducto;
	}
	
	public String devolverProducto()
	{
		return codigoProducto + " " + precio;
	}
}

package ajedrez;

public class Jugar {

	public static void main(String[] args) 
	{
		
		Tablero tablero = new Tablero(8);
		Pieza   p1 = new Pieza("Dama","Negro",1,3),
				p2 = new Pieza("Rey","Negro",1,4),
				p3 = new Pieza("Peon","Negro",2,1),
				p4 = new Pieza("Alfil","Blanco",4,1),
				p5 = new Pieza("Rey","Blanco",4,2);
		
		
		tablero.insertar(p1);
		tablero.insertar(p2);
		tablero.insertar(p3);
		tablero.insertar(p4);
		tablero.insertar(p5);

		
		tablero.escribirFichero("partida.txt");
		//tablero.leerFichero("partida.txt");
		tablero.escribirpPorPantalla();


	}

}

package ajedrez;

public class Pieza {

	private String color;
	private String tipo;
	private int posX, posY;
	
	
	public Pieza (String c, String t, int x, int y)
	{
		color = c;
		tipo = t;
		posX = x;
		posY = y;
	}
	
	public int obtenerX ()
	{
		return posX;
	}
	
	public int obtenerY ()
	{
		return posY;
	}
	
	public String obtenerTipo()
	{
		return tipo;
	}
	
	public String escribir()
	{
		return tipo + "\n" + color + "\n" + posX + "\n" + posY + "\n";
	}
	
}

package ajedrez;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

public class Tablero {

	private Pieza[] lista;
	private int nActual, size;
	
	public Tablero(int s)
	{
		lista = new Pieza[s*s];
		nActual = 0;
		size = s;
	}
	
	public void insertar (Pieza p)
	{
		lista[nActual] = p;
		nActual++;
	}
	
	public void escribirFichero (String nombre)
	{
		try
		{
			File f = new File(nombre);
			FileWriter fw = new FileWriter(f);
			BufferedWriter bw = new BufferedWriter(fw);

			
			for (int i = 0; i < nActual; i++)
			{
				bw.write(lista[i].escribir());
			}
		}
		catch (IOException e)
		{
			System.out.println(e);
		}
	}
	
	public void leerFichero (String nombre)
	{
		try
		{
			
			BufferedReader br = new BufferedReader(new FileReader(nombre));
			String fila;
			String color, tipo;
			int posX, posY;
			
			while ((fila = br.readLine()) != null)
			{
				tipo = fila;
				color = br.readLine();
				posX = Integer.valueOf(br.readLine());
				posY = Integer.valueOf(br.readLine());
				Pieza nueva = new Pieza (tipo, color, posX, posY);
				insertar(nueva);
				fila = br.readLine();
			}
			br.close();
			
		}
		catch (IOException e)
		{
			System.out.println(e);
		}
	}
	
	public Pieza buscarPieza(int px, int py)
	{
		
		Pieza encontrada = null;
		
		for (int i = 0; (i < nActual) && (encontrada == null); i++)
		{
			if ((lista[i].obtenerX() == px) && (lista[i].obtenerY() == py))
				encontrada = lista[i];
		}
		
		return encontrada;
		
	}
	
	
	public void escribirpPorPantalla()
	{
		for (int i = 0; i < size; i++)
		{
			for (int x = 0; x < size; x++)
			{
				Pieza p = buscarPieza(i, x);
				if (p == null)
					System.out.print("[ ]");
				else
					System.out.print("[ " + p.obtenerTipo().charAt(0) + " ]");
				
			}
			System.out.print("\n");
		}
	}
	
}

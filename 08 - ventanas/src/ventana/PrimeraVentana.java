package ventana;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.FlowLayout;
import javax.swing.JLabel;
import javax.swing.BoxLayout;
import java.awt.Color;
import net.miginfocom.swing.MigLayout;
import javax.swing.JComboBox;
import javax.swing.JCheckBox;
import javax.swing.JRadioButton;
import javax.swing.JTextArea;
import javax.swing.JTable;
import javax.swing.JSeparator;
import javax.swing.JEditorPane;
import javax.swing.JPasswordField;
import javax.swing.JToggleButton;
import javax.swing.JSlider;
import javax.swing.JSpinner;
import javax.swing.JList;

public class PrimeraVentana<E> {

	private JFrame frame; 												// Ventana principal. 
	private JPasswordField passwordField;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					PrimeraVentana window = new PrimeraVentana();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public PrimeraVentana() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 535, 363);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JPanel panel = new JPanel();
		panel.setBackground(Color.MAGENTA);
		panel.setBounds(10, 11, 231, 239);
		frame.getContentPane().add(panel);
		panel.setLayout(new MigLayout("", "[217px,grow]", "[14px][22px][23px][23px][][][][][grow]"));
		
		JLabel lblNewLabel = new JLabel("New label");
		panel.add(lblNewLabel, "cell 0 0,alignx left,aligny top");
		
		JComboBox comboBox = new JComboBox();
		panel.add(comboBox, "cell 0 1,growx,aligny top");
		
		JCheckBox chckbxNewCheckBox = new JCheckBox("New check box");
		panel.add(chckbxNewCheckBox, "cell 0 2,alignx left,aligny top");
		
		JRadioButton rdbtnNewRadioButton = new JRadioButton("New radio button");
		panel.add(rdbtnNewRadioButton, "cell 0 3,alignx left,aligny top");
		
		passwordField = new JPasswordField();
		panel.add(passwordField, "cell 0 4,growx");
		
		JToggleButton tglbtnNewToggleButton = new JToggleButton("New toggle button");
		panel.add(tglbtnNewToggleButton, "cell 0 5");
		
		JSlider slider = new JSlider();
		panel.add(slider, "cell 0 6");
		
		JSpinner spinner = new JSpinner();
		panel.add(spinner, "cell 0 7");
		
		JList<? extends E> list = new JList();
		panel.add(list, "cell 0 8,grow");
	}
}

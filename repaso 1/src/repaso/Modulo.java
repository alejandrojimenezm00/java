package repaso;

public class Modulo {

	String nombre;
	int horas;
	boolean convalidable;
	
	Profesor unProfesor;

	public Modulo(String nombre, int horas, boolean convalidable, Profesor unProfesor) {
		//super();
		this.nombre = nombre;
		this.horas = horas;
		this.convalidable = convalidable;
		this.unProfesor = unProfesor;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public int getHoras() {
		return horas;
	}

	public void setHoras(int horas) {
		this.horas = horas;
	}

	public boolean isConvalidable() {
		return convalidable;
	}

	public void setConvalidable(boolean convalidable) {
		this.convalidable = convalidable;
	}

	public Profesor getUnProfesor() {
		return unProfesor;
	}

	public void setUnProfesor(Profesor unProfesor) {
		this.unProfesor = unProfesor;
	}

	@Override
	public String toString() {
		return "modulo [nombre=" + nombre + ", horas=" + horas + ", convalidable=" + convalidable + ", unProfesor="
				+ unProfesor + "]";
	}
	
	
	
	
	
}

package repaso;
import java.util.Scanner;

public class Ejercicio4 {

		public static void main(String[] args) {
			
			int min = 0;
			int max = 100;
			int suma = 0;
			
			try (Scanner in = new Scanner(System.in)) {
				System.out.print("Rango: ");
				int range = in.nextInt();	
				
				int array[][] = new int[range][range];
				
				for (int i = 0; i < range; i++)
					for (int x = 0; x < range; x++)
						array[i][x] = (int)Math.floor(Math.random()*(max-min+1)+min);
				
				for (int i = 0; i < range; i++)
				{
					for (int x = 0; x < range; x++)
						System.out.print(array[i][x]+" ");
					System.out.println();
				}
				
				
				for (int i = 0; i < range; i++)
					for (int x = 0; x < range; x++)
						suma += array[i][x];
			}
			
			
			System.out.print("La suma de los n�meros es: " + suma);
		}
}

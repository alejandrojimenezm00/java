package repaso;

public class alumno extends persona {

	String fechaNacimiento;
	char sexo; // H, M, O
	boolean repetidor;
	
	Modulo unModulo;
	

	public alumno(String dni, String nombre, String apellidos, double salario) {
		super(dni, nombre, apellidos);
		this.salario = 0;
		// TODO Auto-generated constructor stub
	}


	public alumno(String dni, String nombre, String apellidos, double salario, String fechaNacimiento, char sexo,
			boolean repetidor, Modulo unModulo) {
		super(dni, nombre, apellidos, salario);
		this.fechaNacimiento = fechaNacimiento;
		this.sexo = sexo;
		this.repetidor = repetidor;
		this.unModulo = unModulo;
	}


	public String getFechaNacimiento() {
		return fechaNacimiento;
	}


	public void setFechaNacimiento(String fechaNacimiento) {
		this.fechaNacimiento = fechaNacimiento;
	}


	public char getSexo() {
		return sexo;
	}


	public void setSexo(char sexo) {
		this.sexo = sexo;
	}


	public boolean isRepetidor() {
		return repetidor;
	}


	public void setRepetidor(boolean repetidor) {
		this.repetidor = repetidor;
	}


	public Modulo getUnModulo() {
		return unModulo;
	}


	public void setUnModulo(Modulo unModulo) {
		this.unModulo = unModulo;
	}


	@Override
	public String toString() {
		return "alumno [fechaNacimiento=" + fechaNacimiento + ", sexo=" + sexo + ", repetidor=" + repetidor
				+ ", unModulo=" + unModulo + ", dni=" + dni + ", nombre=" + nombre + ", apellidos=" + apellidos
				+ ", salario=" + salario + "]";
	}

}

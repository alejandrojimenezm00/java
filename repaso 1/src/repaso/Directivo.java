package repaso;

public class Directivo extends persona{
 
	String turno;
	boolean salesiano;
	

	public Directivo(String dni, String nombre, String apellidos, double salario, String turno, boolean salesiano) {
		super(dni, nombre, apellidos, salario);
		this.turno = turno;
		this.salesiano = salesiano;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getApellido() {
		return apellidos;
	}

	public void setApellido(String apellido) {
		this.apellidos = apellido;
	}

	public String getDni() {
		return dni;
	}

	public void setDni(String dni) {
		this.dni = dni;
	}

	public String getTurno() {
		return turno;
	}

	public void setTurno(String turno) {
		this.turno = turno;
	}

	public double getSalario() {
		return salario;
	}

	public void setSalario(double salario) {
		this.salario = salario;
	}

	public boolean isSalesiano() {
		return salesiano;
	}

	public void setSalesiano(boolean salesiano) {
		this.salesiano = salesiano;
	}

	@Override
	public String toString() {
		return "Directivo [turno=" + turno + ", salesiano=" + salesiano + ", dni=" + dni + ", nombre=" + nombre
				+ ", apellidos=" + apellidos + ", salario=" + salario + "]";
	}


	
	
	
	
	
}

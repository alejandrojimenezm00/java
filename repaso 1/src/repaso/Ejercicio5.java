package repaso;
import java.util.Scanner;


public class Ejercicio5 {

	public static void main(String[] args) {
		
		try (Scanner in = new Scanner(System.in)) {
			System.out.print("Dime un n�mero: ");
			int num = in.nextInt();
			
			System.out.println("El factorial de " + num + " es: " + sumador(num));
		}

	}
	
	public static int sumador(int num) {
		
		return num != 0 ? num * sumador(num-1) : 1;
		
	}

}

package repaso;

public class Profesor extends persona{ //para heredar a�adimos la palabra extends seguido de la clase que va a herdar
	
	// Definicion de las variables que tendra el objeto
	
	// String dni, nombre, apellidos;
	// double salario;
	int numAsignaturas;
	boolean tutor;
	
	
	// Constructor del objeto
	
	/*
	public Profesor(String dni, String nombre, String apellidos, double salario, int numAsignaturas, boolean tutor) {
		//super();
		this.dni = dni;
		this.nombre = nombre;
		this.apellidos = apellidos;
		this.salario = salario;
		this.numAsignaturas = numAsignaturas;
		this.tutor = tutor;
	}
	*/ // Constructor comentado para usar herencia del ejercicio 7, en caso de no usar herencia descomentar este y las variables

	
	
	// Constructor con herencia
	public Profesor(String dni, String nombre, String apellidos, double salario, int numAsignaturas, boolean tutor) {
		super(dni, nombre, apellidos, salario);
		this.numAsignaturas = numAsignaturas;
		this.tutor = tutor;
	}

	public int getNumAsignaturas() {
		return numAsignaturas;
	}


	public void setNumAsignaturas(int numAsignaturas) {
		this.numAsignaturas = numAsignaturas;
	}


	public boolean isTutor() {
		return tutor;
	}


	public void setTutor(boolean tutor) {
		this.tutor = tutor;
	}



	// Para pasar a string los elementos del objeto
	
	@Override
	public String toString() {
		return "Profesor [numAsignaturas=" + numAsignaturas + ", tutor=" + tutor + ", dni=" + dni + ", nombre=" + nombre
				+ ", apellidos=" + apellidos + ", salario=" + salario + "]";
	}
	
	
	
	
	

}

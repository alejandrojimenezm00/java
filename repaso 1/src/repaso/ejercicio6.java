package repaso;

public class ejercicio6 {

	public static void main(String[] args) {
		
		//Definicion de objetos
		
		Profesor profesor1 = new Profesor("111222333G", "Antonio", "Perez", 1102, 5, false);
		Profesor profesor2 = new Profesor("444555666H", "Maria", "Martinez", 1111, 6, true);
		
		Administracion admin1 = new Administracion("Marcos", "Delgado", "777888999Y", 10, "grado medio", 22245);
		Administracion admin2 = new Administracion("Matilde", "Fernandez", "000333444I", 4, "EGB", 1500);
		
		Directivo dir1 = new Directivo("David", "Moyano", "123456789M", 2890, "Tarde", false);
		Directivo dir2 = new Directivo("Juan", "mu�oz", "098765432O", 960, "Ma�ana", true);
	
		alumno a1 = new alumno("09101562W", "Antonio", "Perez", 0, "17/12/2000", 'H', false, new Modulo("fisica", 10, false, profesor2));
		
		System.out.println(profesor1);
		
		// operaciones con los objetos
		
		profesor1.setSalario(100012);
		System.out.println(profesor1);
		System.out.println(profesor2);
		System.out.println(admin1);
		System.out.println(admin2);
		System.out.println(dir1);
		System.out.println(dir2);
		System.out.println(a1);
		
		

	}
	

}

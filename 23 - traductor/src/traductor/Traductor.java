package traductor;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import java.net.*;
import java.io.*;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class Traductor extends JFrame {

	private JPanel contentPane;
	private JTextField textFieldPalabra;

	/**
	 * Create the frame.
	 */
	public Traductor() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblNewLabel = new JLabel("Palabra");
		lblNewLabel.setBounds(63, 70, 46, 14);
		contentPane.add(lblNewLabel);
		
		textFieldPalabra = new JTextField();
		textFieldPalabra.setBounds(119, 67, 86, 20);
		contentPane.add(textFieldPalabra);
		textFieldPalabra.setColumns(10);
		
		JLabel labelTraduccion = new JLabel("Tu palabra traducida es: ");
		labelTraduccion.setBounds(63, 139, 361, 14);
		contentPane.add(labelTraduccion);
		
		JButton buttonTraducir = new JButton("Traducir");
		buttonTraducir.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				String palabra = textFieldPalabra.getText();
				
				if (textFieldPalabra.getText().isEmpty())
					labelTraduccion.setText("Escribe algo antes de traducir");
				else
				{
					
					labelTraduccion.setText("Tu palabra traducida es: " + Traducir.traducirWeb(palabra));
				}

			}
		});
		buttonTraducir.setBounds(116, 98, 89, 23);
		contentPane.add(buttonTraducir);
	}
	
	

	
	public static void main(String[] args) throws Exception {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Traductor frame = new Traductor();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
		
        
	}
}


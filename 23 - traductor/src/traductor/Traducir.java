package traductor;

import java.io.IOException;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

public class Traducir {

	
	static String traducirWeb(String palabra) {
	String cadena = null;		
	Document document = null;            
	String webPage = "https://www.ingles.com/traductor/"+palabra; 
		try {
			document = Jsoup.connect(webPage).get();
		} catch (IOException e) {
			e.printStackTrace();
		}	
        Elements palabra2 = document.getElementById("quickdef1-es").getElementsByClass("_3Le9u96E");
        cadena=palabra2.html().toUpperCase();
	return cadena;
	}	
}

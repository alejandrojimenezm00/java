package arrays;

public class Arrays {

	public static void main(String[] args)
	{
		
		// uso normal de arrays
		
		/*
		int[] tabla = new int[5]; // tabla de 5 enteros. En java se crea primero el puntero a la tabla y luego el tama�o
										// Las tablas en java son objetos, a diferencia de c que son variables
		
		int[] tabla2 = {1,3,4,5,8};
		
		for (int i = 0; i < 5; i++)
		{
			tabla[i] = i+1;
		}

		for (int i = 0; i < 5; i++)
			System.out.println(i + " -> " + tabla[i]);
		
		System.out.println("\n");

		for (int i = 0; i < 5; i++)
			System.out.println(i + " -> " + tabla2[i]);
			
		
		*/
		
		
		// arrays con objetos
		
		Persona[] tabla; 		// declaraci�n del array tipo persona
		tabla = new Persona[5]; // creaci�n del array en el que habr� 5 personas
		
		for (int i = 0; i < 5; i++)
		{
			tabla[i] = new Persona("Pepe", i);
		}
		
		for (int i = 0; i < 5; i++)
		{
			System.out.println(tabla[i].escribir());
		}
		
	}
	

}

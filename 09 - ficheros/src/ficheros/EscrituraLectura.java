package ficheros;

import java.io.*;

public class EscrituraLectura {

	
	public static void EscribirDatos(String nombre, String[] datos)
	{
		// 1� Creaci�n del fichero de escritura
		try
		{

			File 		f = new File(nombre); 	// Creamos un ficher
			FileWriter fw = new FileWriter(f);  // fileWriter para escribir en el fichero
		
			for (int i = 0; i < datos.length;i++)
			{
				for (int j = 0; j < datos[i].length();j++)
				{
					fw.write(datos[i].charAt(j)); //datos [i][j]
				}
				fw.write('\r');  // retorno de carra
				fw.write('\n');  // salto de linea
			}
			
			fw.close();
		
		}
		
		catch (IOException e)
		{
			System.out.println("Error al escribir datos: " + e.getMessage());
		}
		
	}
	
	
	public static void LeerDatos(String nombre, String[] datos)
	{
		
		try
		{
			// 1. Creacion del fichero de lectura
			File f = new File(nombre);
			FileReader fr = new FileReader(f);
			int caracter;
			String valor = "";
			// char[] letra = new char[20];
			
			int contador = 0;
			
			// 2. Lectura l�nea a l�nea de cada nombre
			caracter = fr.read();
		
			while (caracter != -1)
			{
				if (caracter == '\n')
				{
					datos[contador] = valor;						// Una vez que se encuentre un \n meter� en el array que le hemos pasado en una posici�n auxiliar
					contador++;										// determinada por la variable contador
					valor = "";										// Despu�s de esto se restablecer� la variable valor
				}
				
				if (caracter != '\n')
				{
					valor += (char) caracter;						// almacenamos en valor el contenido de aquello que tenga carcter
					
				}
				
				caracter = fr.read();								// Por cada vuelta del bucle, se leer� un caracter tras otra hasta leer un EOF.
			}
			
			fr.close();
		
			
		}
		catch (IOException e)
		{
			System.out.println("Error al imprimir datos " + e);
		}
		
		
		
	}
	
	
	public static void main(String[] args) 
	{
		String[] misNombres = { "Pepe", "Ana", "Luis" };  // Escritura
		String[] misdatosLeidos = new String[3];		  // lectura avanzada
		
		EscribirDatos("Mis datos.txt", misNombres);
		LeerDatos("Mis datos.txt", misdatosLeidos);
		
		for (int i = 0; i < 3; i++)
			System.out.println(misdatosLeidos[i]);
		
		

	}

}

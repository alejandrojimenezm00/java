package almacen;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.Scanner;

public class Operar {

	public static void main(String[] args) {

		/*
		Articulos a1 = new Articulos("camiseta", 2, 1),
				  a2 = new Articulos("pantalon", 4, 1),
				  a3 = new Articulos("sudadera", 3, 2),
				  a4 = new Articulos("abrigo", 7, 2);

		*/
		
		int ancho = 0, largo = 0, campoActual = 3;
		String nombre = "";
		Scanner entrada = new Scanner(System.in);
		
		Articulos[] listaArticulos = new Articulos[7];
		
		for (int i = 0; i < 3; i++)
		{
			System.out.println("Nombre del producto");
			nombre = entrada.nextLine();
			System.out.println("Acho del producto");
			ancho = entrada.nextInt();
			entrada.nextLine();
			System.out.println("Largo del producto");
			largo = entrada.nextInt();
			entrada.nextLine();
			
			listaArticulos[i] = new Articulos(nombre, ancho, largo);
			
		}
		
		
		try
		{
			
			BufferedReader br = new BufferedReader(new FileReader("almacen.txt"));
			
			String linea = br.readLine();
			String[] campo;
			
			while (linea != null)
			{
				
				campo = linea.split(";");
				listaArticulos[campoActual] = new Articulos(campo[0], Integer.valueOf(campo[1]), Integer.valueOf(campo[2]));
				campoActual++;
				
				for (int i = 0; i < campo.length; i++)
					campo[i] = "";
				
				linea = br.readLine();
			}
			br.close();
			
		}
		catch (IOException e)
		{
			System.out.println(e);
		}
		
		for (int i = 0; i < 7; i++)
			if (listaArticulos[i].getAncho() > 7)
				listaArticulos[i].escalarMayor7();
		
		listaArticulos[2].disminuirAncho(2);
		listaArticulos[1].escalarMedidas(10);
		
		
		for (int i = 0; i < 7; i++)
			System.out.println(listaArticulos[i].imprimirPorPantalla());
		
		
		
	}

}

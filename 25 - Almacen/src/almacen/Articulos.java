package almacen;

public class Articulos {

	private String nombre;
	private int ancho, largo;
	
	public Articulos (String n, int a, int l)
	{
		nombre = n;
		ancho = a;
		largo = l;
	}
	
	public int getAncho()
	{
		return ancho;
	}
	
	public void disminuirAncho (int num)
	{
		ancho -= num;
	}
	
	public void escalarMedidas (int num)
	{
		ancho *= num;
		largo *= num;
	}
	
	public void escalarMayor7 ()
	{
		ancho *= 2;
		largo *= 2;
	}
	
	public String imprimirPorPantalla ()
	{
		return nombre + " " + ancho + " " + largo;
	}
	
}

package personas;


import java.io.*;


public class TablaPersona 
{
	private Persona[] tablaPersona;
	private int posicionActual;
	private IndiceNombre[] tablaIndicesNombre;
	
	
	public TablaPersona()
	{
		tablaPersona = new Persona[10]; 
		posicionActual = 0;
		tablaIndicesNombre = new IndiceNombre[10];
	}
	
	public void inicializar()
	{
		for(int i=0;i<tablaPersona.length;i++)
		{
			tablaPersona[i] = new Persona();
		}
		for(int i=0;i<tablaIndicesNombre.length;i++)
		{
			tablaIndicesNombre[i] = new IndiceNombre();
		}
		posicionActual = 0;
	}
	
	public void insertar(Persona p)
	{
        tablaPersona[posicionActual] = p;
        posicionActual++;
	}
	
	public void escribirDatos()
	{
		if (posicionActual==0)
		{
			System.out.println("No hay datos en la tabla");
		}
		else
		{
			for(int i=0;i<posicionActual;i++)
			{
				System.out.println(tablaPersona[i].escribirNombre());
			}
		}
	}
	
	public Persona mayorEdad()
	{
		Persona p = null;
		int edadMayor = 0;
		
		for(int i=0;i<posicionActual;i++)
		{
			if (edadMayor < tablaPersona[i].obtenerEdad())
			{
				edadMayor = tablaPersona[i].obtenerEdad();
				p = tablaPersona[i];
			}
		}
		return p;
	}
	
	public Persona mayorDireccion()
	{
		Persona p = null;
		int direccionMayor = 0;
		
		for(int i=0;i<posicionActual;i++)
		{
			if (direccionMayor < tablaPersona[i].obtenerDireccion().length() )
			{
				direccionMayor = tablaPersona[i].obtenerDireccion().length();
				p = tablaPersona[i];
			}
		}
		return p;
	}
	
	public void eliminarPosicion(int pos)
	{
		for(int i=pos;i<(posicionActual-1);i++)
		{
			tablaPersona[i] = tablaPersona[i+1];
		}
		tablaPersona[posicionActual] = null;
		posicionActual--;
	}

	public void eliminarRango(int posInicial,int posFinal)
	{
		for(int i=1;i<=(posFinal-posInicial)+1;i++)
			eliminarPosicion(posInicial);
	}
	
	public void eliminarRangoV2(int posInicial,int posFinal)
	{
		for(int i=posFinal-posInicial;i>=0;i--)
		{
			for(int j=posInicial;j<(posicionActual-1);j++)
			{
				tablaPersona[j] = tablaPersona[j+1];
			}
			tablaPersona[posicionActual] = null;
			posicionActual--;	
		}
	}

	public void eliminarRangoV3(int posInicial,int posFinal)
	{
		int v = (posFinal - posInicial)+1;  
		for(int i=posInicial;i<posicionActual;i++)
		{
		   tablaPersona[i] = tablaPersona[i+v];
		}
		posicionActual -= v;	
	}

	public void insertarPosicion(int pos)
	{
		for(int i=posicionActual;i>pos;i--)
		{
			tablaPersona[i] = tablaPersona[i-1];
		}
		tablaPersona[pos] = new Persona(); 
		// No poner null para que la escritura de la tabla
		// con un objeto null devuelva un error
		posicionActual++;
	}

	public void insertarRango(int posInicial,int posFinal)
	{
		for(int i=1;i<=(posFinal-posInicial)+1;i++)
			insertarPosicion(posInicial);
	}
	
	public void crearTablaIndices()
	{
		IndiceNombre indice;
		for(int i=0;i<posicionActual;i++)
		{
			indice = new IndiceNombre(tablaPersona[i].obtenerNombre(),
							         i);
			tablaIndicesNombre[i] = indice;
		}
	}
	
	public void ordenarTablaIndices()
	{
		for(int i=0;i<posicionActual-1;i++)
		{
			for(int j=0;j<posicionActual-1-i;j++)
			{
				if ( tablaIndicesNombre[j].getNombre().compareTo(						
						 tablaIndicesNombre[j+1].getNombre()) > 0 )		
				{
					IndiceNombre aux = tablaIndicesNombre[j];
					tablaIndicesNombre[j] = tablaIndicesNombre[j+1];
					tablaIndicesNombre[j+1] = aux;
				}
			}
		}
	}
	
	public void mostrarTablaIndices()
	{
		int posTabla;
		for(int i=0;i<posicionActual;i++)
		{
			posTabla = tablaIndicesNombre[i].getPosicion();
			System.out.println("Indice: " + tablaIndicesNombre[i].getNombre() + 
			                   "Posicion: " + posTabla);
			}
	}
	
	public void escribirCadena(FileWriter fw,String cadena) 
	{
		try
		{
			for(int i=0;i<cadena.length();i++)
			{
				fw.write(cadena.charAt(i));
			}
		}
		catch(IOException e)
		{
			System.out.println("Error escritura cadena: " + e.getMessage());
		}
	}
	
	public String leerCadena(FileReader fr,int l)
	{
		try
		{
			String cadena="";
			
			while (l!='\n')
			{
				cadena += (char)l;
				l = fr.read();
			}
			return cadena;
		}
		catch(IOException e)
		{
			System.out.println("Error al leer: " + e.getMessage());
			return "";
		}
	}
	
	
	public void guardarTabla(String nombre)
	{
		try
		{
			File f = new File(nombre);
			FileWriter fw = new FileWriter(f);
			
			// Formato de fichero: un valor en cada l�nea y con salto
			for(int i=0;i<posicionActual;i++)
			{
				// Persona: nombre, apellido, edad, direccion
				escribirCadena(fw,tablaPersona[i].obtenerNombre());
				escribirCadena(fw,"\n");
				escribirCadena(fw,tablaPersona[i].obtenerApellido());
				escribirCadena(fw,"\n");
				escribirCadena(fw,String.valueOf(tablaPersona[i].obtenerEdad()));
				escribirCadena(fw,"\n");
				escribirCadena(fw,tablaPersona[i].obtenerDireccion());
				escribirCadena(fw,"\n");
			}
			fw.close();
		}
		catch(IOException e)
		{
			System.out.println("Error escribir fichero: " + e.getMessage());
		}
	}

	public void leerTabla(String nombre)
	{
		try
		{
			File f = new File(nombre);
			FileReader fr = new FileReader(f);
			int letra;
			String nombreP,apellido,direccion,edad;
			int    valorEdad;
			
		    letra = fr.read();
		    while(letra != -1)
		    {
				// Persona: nombre, apellido, edad, direccion
		    	nombreP = leerCadena(fr,letra);
			    letra = fr.read();
			    apellido = leerCadena(fr,letra);
			    letra = fr.read();
			    edad = leerCadena(fr,letra);
			    valorEdad = Integer.parseInt(edad);
			    letra = fr.read();
			    direccion = leerCadena(fr,letra);
			    
			    Persona p = new Persona(nombreP,apellido,
			    		                valorEdad,
			    		                direccion);
			    insertar(p);
			    letra = fr.read();
		    }
			fr.close();
		}
		catch(IOException e)
		{
			System.out.println("Error escribir fichero: " + e.getMessage());
		}
	}
	
	public void guardarTablaV2(String nombre)
	{
		try
		{
			File f = new File(nombre);
			FileWriter fw = new FileWriter(f);
			BufferedWriter bw = new BufferedWriter(fw);  
			
			// Formato de fichero: un valor en cada l�nea y con salto
			for(int i=0;i<posicionActual;i++)
			{
				// Persona: nombre, apellido, edad, direccion
				bw.write(tablaPersona[i].obtenerNombre());
				bw.write("\n");
				bw.write(tablaPersona[i].obtenerApellido());
				bw.write("\n");
				bw.write(String.valueOf(tablaPersona[i].obtenerEdad()));
				bw.write("\n");
				bw.write(tablaPersona[i].obtenerDireccion());
				bw.write("\n");
			}
			bw.close();
		}
		catch(IOException e)
		{
			System.out.println("Error escribir fichero: " + e.getMessage());
		}
	}

	public void leerTablaV2(String nombre)
	{
		try
		{
			File f = new File(nombre);
			FileReader fr = new FileReader(f);
			BufferedReader br = new BufferedReader(fr);
			String linea;
			String nombreP,apellido,direccion,edad;
			int    valorEdad;
			
		    linea = br.readLine();
		    while(linea != null)
		    {
				// Persona: nombre, apellido, edad, direccion
		    	nombreP = linea;
		    	apellido = br.readLine();
		    	edad = br.readLine();
			    valorEdad = Integer.parseInt(edad);
		    	direccion = br.readLine();
			    Persona p = new Persona(nombreP,apellido,
			    		                valorEdad,
			    		                direccion);
			    insertar(p);
			    linea = br.readLine();
		    }
			br.close();
		}
		catch(IOException e)
		{
			System.out.println("Error escribir fichero: " + e.getMessage());
		}
	}
	public void guardarTablaV3(String nombre)
	{
		try
		{
			File f = new File(nombre);
			FileWriter fw = new FileWriter(f);
			BufferedWriter bw = new BufferedWriter(fw);  
			
			// Formato de fichero: un valor en cada l�nea y con salto
			for(int i=0;i<posicionActual;i++)
			{
				// Persona: nombre, apellido, edad, direccion
				bw.write(tablaPersona[i].obtenerNombre());
				bw.write(";");
				bw.write(tablaPersona[i].obtenerApellido());
				bw.write(";");
				bw.write(String.valueOf(tablaPersona[i].obtenerEdad()));
				bw.write(";");
				bw.write(tablaPersona[i].obtenerDireccion());
				bw.write(";\n");
			}
			bw.close();
		}
		catch(IOException e)
		{
			System.out.println("Error escribir fichero: " + e.getMessage());
		}
	}

	public void leerTablaV3(String nombre)
	{
		try
		{
			File f = new File(nombre);
			FileReader fr = new FileReader(f);
			BufferedReader br = new BufferedReader(fr);
			String linea;
			String campo,nombreP="",apellido="",direccion="",edad="";
			int    nCampo=1;
			int    valorEdad;
			
		    campo = "";
			linea = br.readLine();
		    while(linea != null)
		    {
	       	    // La linea nombre;apellido;edad;direccion
		    	nCampo=1;
		    	for(int i=0;i<linea.length();i++)
		    	{
		    		if (linea.charAt(i)!=';')
		    			campo += linea.charAt(i);
		    		else
		    		{
		    			switch(nCampo)
		    			{
		    			   case 1: nombreP = campo;
		    			           break;
		    			   case 2: apellido = campo;
    			           		   break;
		    			   case 3: edad = campo;
    			                   break;
		    			   case 4: direccion = campo;
    			                   break;
		    			}
		    			nCampo++;
		    			campo="";
		    		}
		    	}
			    valorEdad = Integer.parseInt(edad);
			    Persona p = new Persona(nombreP,apellido,
			    		                valorEdad,
			    		                direccion);
			    insertar(p);
			    linea = br.readLine();
		    }
			br.close();
		}
		catch(IOException e)
		{
			System.out.println("Error escribir fichero: " + e.getMessage());
		}
	}
	
	
	public void guardarTablaIndices(String nombre)
	{
		try
		{
			int posTabla;
			File f = new File(nombre);
			FileWriter fw = new FileWriter(f);
			BufferedWriter bw = new BufferedWriter(fw);  
			
			// Formato de fichero: un valor en cada l�nea y con salto
			for(int i=0;i<posicionActual;i++)
			{
				posTabla = tablaIndicesNombre[i].getPosicion();
				
				bw.write(tablaPersona[i].obtenerNombre());
				bw.write(posTabla);
				bw.write(" \n");

			}
			bw.close();
		}
		catch(IOException e)
		{
			System.out.println("Error escribir fichero: " + e.getMessage());
		}
	}
	
	public void leerTablaIndices(String nombre)
	{
		try
		{
			int posTabla;
			File f = new File(nombre);
			FileWriter fw = new FileWriter(f);
			BufferedWriter bw = new BufferedWriter(fw);  
			
			// Formato de fichero: un valor en cada l�nea y con salto
			for(int i=0;i<posicionActual;i++)
			{
				posTabla = tablaIndicesNombre[i].getPosicion();
				
				bw.write(tablaPersona[i].obtenerNombre());
				bw.write(posTabla);
				bw.write(" \n");

			}
			bw.close();
		}
		catch(IOException e)
		{
			System.out.println("Error escribir fichero: " + e.getMessage());
		}
	}
	
	
	
	
	
}
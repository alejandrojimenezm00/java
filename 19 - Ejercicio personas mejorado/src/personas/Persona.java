package personas;

public class Persona
{
	private String nombre;
	private String apellidos;
	private int    edad;
	private String direccion;
	
	public Persona()
	{
		nombre="";
		apellidos="";
		edad=0;
		direccion="";
	}
	
	public Persona(String n,String a,int e,String d)
	{
		nombre=n;
		apellidos=a;
		edad=e;
		direccion=d;
	}

	public Persona(String n,String a)
	{
		nombre=n;
		apellidos=a;
		edad=0;
		direccion="";
	}
	
	public String obtenerNombre()
	{
		return nombre;
	}
	
	public String obtenerNombreApellidos()
	{
		return nombre + " " + apellidos;
	}
	
	public String obtenerApellido()
	{
		return apellidos;
	}
	
	
	public int obtenerEdad()
	{
		return edad;
	}
	
	public String obtenerDireccion()
	{
		return direccion;
	}
	
	public boolean mayorEdad()
	{
		if (edad>=18)
			return true;
		else
			return false;
	}
	
	public String escribirNombre() 
	{
		return nombre + " " + apellidos + " " + direccion + " " + edad;
	}
	
	public String escribirApellidos() 
	{
		return apellidos + " " + nombre + " " + direccion + " " + edad;
	}
	
	
	
	

}

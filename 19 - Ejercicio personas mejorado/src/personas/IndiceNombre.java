package personas;

public class IndiceNombre 
{
	private String nombre;
	private int posicion;
	
	public IndiceNombre()
	{
		nombre = "";
		posicion = -1;
	}
	
	public IndiceNombre(String n,int p)
	{
		nombre = n;
		posicion = p;
	}
	
	public String getNombre()
	{
		return nombre;
	}
	
	public int getPosicion()
	{
		return posicion;
	}
}

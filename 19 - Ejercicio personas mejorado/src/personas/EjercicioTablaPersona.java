package personas;

import java.util.*;

public class EjercicioTablaPersona {

	public static void menu()
	{
		System.out.println("MENU DE OPCIONES");
		System.out.println("1.  Inicializar");
		System.out.println("2.  Leer Datos");
		System.out.println("3.  Escribir  tabla");
		System.out.println("4.  Mayor edad");
		System.out.println("5.  Direccion mas larga");
		System.out.println("6.  Eliminar posici�n");
		System.out.println("7.  Eliminar rango posici�n");
		System.out.println("8.  Insertar posici�n");
		System.out.println("9.  Insertar rango posici�n");
		System.out.println("10. Crear indice nombres");
		System.out.println("11. Escribir tabla indice nombres");
		System.out.println("12. Guardar tabla en fichero-SinBuffer");
		System.out.println("13. Leer tablad del fichero-SinBuffer");
		System.out.println("14. Guardar tabla en fichero-ConBuffer");
		System.out.println("15. Leer tablad del fichero-ConBuffer");
		System.out.println("16. Guardar tabla en fichero-ConBuffer-;");
		System.out.println("17. Leer tablad del fichero-ConBuffer-;");
		System.out.println("18. Leer tablad del fichero-Indices");
		System.out.println("19. Salir");
	}
	
	public static void main(String[] args) 
	{
		TablaPersona miTabla;
		String nombre,direccion,apellido;
		int edad,posicion,posicionFinal;
		Persona persona;

		miTabla = new TablaPersona();
		Scanner entrada = new Scanner(System.in);
		int opcion;
		do
		{
		   menu();
		   System.out.println("Introduce opci�n: ");
		   opcion = entrada.nextInt();
		   entrada.nextLine(); 
		   switch(opcion)
		   {
		     case 1: miTabla.inicializar();
		             break;
		     case 2: System.out.println("Introduce nombre: ");
		             nombre = entrada.nextLine();
		             System.out.println("Introduce apellido: ");
		             apellido = entrada.nextLine();
		             System.out.println("Introduce direcci�n: ");
		             direccion = entrada.nextLine();
		             System.out.println("Introduce edad: ");
		             edad = entrada.nextInt();
		             persona = new Persona(nombre,apellido,edad,direccion);
		             miTabla.insertar(persona);
		             break;
		     case 3: miTabla.escribirDatos();
		             break;
		     case 4: persona = miTabla.mayorEdad();
		             System.out.println(persona.escribirNombre());
		             break;
		     case 5: persona = miTabla.mayorDireccion();
                     System.out.println(persona.escribirNombre());
                     break;
		     case 6: System.out.println("Introduce posici�n: ");
                     posicion = entrada.nextInt();
		    	     miTabla.eliminarPosicion(posicion);
                     break;
		     case 7: System.out.println("Introduce posici�n inicial: ");
                     posicion = entrada.nextInt();
                     System.out.println("Introduce posici�n final: ");
                     posicionFinal = entrada.nextInt();
		    	     miTabla.eliminarRangoV2(posicion,posicionFinal);
		             break;
		     case 8: System.out.println("Introduce posici�n: ");
		             posicion = entrada.nextInt();
		    	     miTabla.insertarPosicion(posicion);
		    	     break;
		     case 9: System.out.println("Introduce posici�n inicial: ");
		             posicion = entrada.nextInt();
		             System.out.println("Introduce posici�n final: ");
		             posicionFinal = entrada.nextInt();
		    	     miTabla.insertarRango(posicion, posicionFinal);
		    	     break;
		     case 10: miTabla.crearTablaIndices();
		             miTabla.ordenarTablaIndices();
		             break;
		     case 11: miTabla.mostrarTablaIndices();
		             break;
		     case 12: miTabla.guardarTabla("TablaP-SinBuffer.txt");
                      break;
		     case 13: miTabla.leerTabla("TablaP-SinBuffer.txt");
                      break;
		     case 14: miTabla.guardarTablaV2("TablaP-ConBuffer.txt");
             		  break;
		     case 15: miTabla.leerTablaV2("TablaP-ConBuffer.txt");
                      break;
		     case 16: miTabla.guardarTablaV3("TablaP-ConComa.txt");
    		          break;
             case 17: miTabla.leerTablaV3("TablaP-ConComa.txt");
                      break;
             case 18: miTabla.guardarTablaIndices("TablaP-indices.txt");
             		  break;
             		  
		   }
		}while (opcion!=19);
	}

}

package peaje;

import java.io.*;

public class Peaje {

	private Vehiculo[] vehiculosRegistrados;
	private int totalRecaudado, totalOcupantes, posVehiculo;
	
	public Peaje ()
	{
		vehiculosRegistrados = new Vehiculo[20];
		
		for (int i = 0; i < 20; i++)
			vehiculosRegistrados[i] = new Vehiculo();
		
		totalOcupantes = 0;
		totalRecaudado = 0;
		posVehiculo = 0;
	}
	
	public void insertarDatos (String t, String m, int o)
	{
		vehiculosRegistrados[posVehiculo]= new Vehiculo(t, m, o);
		
		switch (t)
		{
		case "turismo": totalRecaudado += 15;
						break;
		case "camion": totalRecaudado += 20;
					   break;
		case "colectivo": totalRecaudado += 10;
						  break;
		}
		
		totalOcupantes += o;
		posVehiculo++;
	}
	
	public int getDineroRecaudado()
	{
		return totalRecaudado;
	}
	
	public int totalOcupantes()
	{
		return totalOcupantes;
	}
	
	public int totalvehiculos()
	{
		return posVehiculo;
	}
	
	public int totalCamiones()
	{
		int numCamiones = 0;
		
		for (int i = 0; i < posVehiculo; i++)
			if (vehiculosRegistrados[i].obtenerNombre().equals("camion"))
				numCamiones++;
		
		return numCamiones;
	}
	
	public Vehiculo masOcupantes()
	{
		Vehiculo masOcupado = new Vehiculo();
		
		for (int i = 0; i < posVehiculo; i++)
			if (vehiculosRegistrados[i].obtenerOcupantes() > masOcupado.obtenerOcupantes())
				masOcupado = vehiculosRegistrados[i];
		
		
		return masOcupado;
	}
	
	public void guardarDatos()
	{
				
		try {
			
		
				BufferedWriter br = new BufferedWriter(new FileWriter("registro - con buffer.txt", true));
				
				for (int i = 0; i < posVehiculo; i++)
				{
					br.write(vehiculosRegistrados[i].imprimirVehiculo());
					br.write("\n");
				}
				
				br.close();
		
		
			} 
			catch (FileNotFoundException e) 
			{
				System.out.println(e);
			} 
			catch (IOException e)
			{
				System.out.println(e);
			}
		
		
		
	}
	
	public void escribirCadena(FileWriter fw, String cadena)
	{
		try
		{
			
			for (int i = 0; i < cadena.length(); i++)
				fw.write(cadena.charAt(i));
			
		}
		catch (IOException e)
		{
			System.out.println(e);
		}
	}
	
	public void guardarDatosSinBuffer()
	{
		
		try 
		{
			
			FileWriter fr = new FileWriter("registro - sin buffer.txt", true);
			
			for (int i = 0; i < posVehiculo; i++)
			{
				escribirCadena(fr, vehiculosRegistrados[i].obtenerNombre());
				escribirCadena(fr, ";");
				escribirCadena(fr, vehiculosRegistrados[i].obtenerMatricula());
				escribirCadena(fr, ";");
				escribirCadena(fr, String.valueOf(vehiculosRegistrados[i].obtenerNombre()));
				escribirCadena(fr, ";");
				escribirCadena(fr, "\n");
			}
			fr.close();
			
			
		} 
		catch (IOException e) 
		{
			e.printStackTrace();
		}
		
		
	}
	
}

package peaje;

import java.util.*;

public class Autopista {

	public static void main(String[] args) {

		int eleccion = 0;
		@SuppressWarnings("resource")
		Scanner entrada = new Scanner(System.in);
		
		int numPeaje, ocupantes;
		String matricula, tipo;
		
		Peaje[] peajes = new Peaje[4];
		
		for (int i = 0; i < 4; i++)
			peajes[i] = new Peaje();
		
		do
		{
			System.out.println("0 -> Ingresar Vehiculo");
			System.out.println("1 -> Escribir cantidad total Vehiculos");
			System.out.println("2 -> Escribir cantidad total recaudada");
			System.out.println("3 -> Escribir cantidad total ocupantes");
			System.out.println("4 -> Escribir datos vehiculo mas ocupantes");
			System.out.println("5 -> Escribir datos camiones primer peaje");
			System.out.println("6 -> Guardar datos - buffer");
			System.out.println("7 -> Guardar Datos - sin buffer");
			System.out.println("8 -> Salir");
			System.out.println("Opcion:");		           
			
			eleccion = entrada.nextInt();
			entrada.nextLine();
			
			switch (eleccion)
			{
			case 0:
					System.out.println("Peaje ocupado");
					numPeaje = entrada.nextInt();
					entrada.nextLine();
					System.out.println("Tipo de vehiculo");
					tipo = entrada.nextLine();
					System.out.println("Matricula");
					matricula = entrada.nextLine();
					System.out.println("ocupantes");
					ocupantes = entrada.nextInt();
					entrada.nextLine();
					
					peajes[numPeaje].insertarDatos(tipo, matricula, ocupantes);
					break;
					
			case 1:
					for (int i = 0; i < 4; i++)
						System.out.println("En el peaje " + i + " han pasado: " + peajes[i].totalvehiculos() + " vehiculos");
					break;
			case 2: 
					for (int i = 0; i < 4; i++)
						System.out.println("En el peaje " + i + " se ha recaudado: " + peajes[i].getDineroRecaudado() + " euros");
					break;
			case 3: 
					for (int i = 0; i < 4; i++)
						System.out.println("En el peaje " + i + " han pasado: " + peajes[i].totalOcupantes() + " personas");
					break;
			case 4:
					Vehiculo masOcupado = new Vehiculo();
					
					for (int i = 0; i < 4; i++)
						if (peajes[i].masOcupantes().obtenerOcupantes() > masOcupado.obtenerOcupantes())
							masOcupado = peajes[i].masOcupantes();
					
					System.out.println(masOcupado.imprimirVehiculo());
					
					break;
			case 5:
					Vehiculo[] camiones = new Vehiculo[peajes[0].totalCamiones()];
					
					for (int i = 0; i < camiones.length; i++)
						System.out.println(camiones[i].imprimirVehiculo());
			case 6:
					for (int i = 0; i < 4; i++)
						peajes[i].guardarDatos();
				    break;
			case 7: 
				    for (int i = 0; i < 4; i++)
				    	peajes[i].guardarDatosSinBuffer();
				    break;
			}
			
			
		}while (eleccion != 9);
		
	}

}

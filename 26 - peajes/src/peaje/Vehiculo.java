package peaje;

public class Vehiculo {

	private String tipo, matricula;
	private int ocupantes;
	
	public Vehiculo (String t, String m, int o)
	{
		tipo = t;
		matricula = m;
		ocupantes = o;
	}
	
	public Vehiculo ()
	{
		tipo = "";
		matricula = "";
		ocupantes = 0;
	}
	
	public String obtenerNombre()
	{
		return tipo;
	}
	
	public String obtenerMatricula()
	{
		return matricula;
	}
	
	public int obtenerOcupantes()
	{
		return ocupantes;
	}
	
	public String imprimirVehiculo()
	{
		return tipo + ";" + matricula + ";" + ocupantes + ";";
	}
}
